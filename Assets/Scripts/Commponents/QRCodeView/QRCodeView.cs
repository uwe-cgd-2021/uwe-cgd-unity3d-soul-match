using UnityEngine;
using UnityEngine.UI;
using ZXing;
using ZXing.QrCode;



[RequireComponent(typeof(RawImage))]
public class QRCodeView : MonoBehaviour
{
    private RawImage imageView = null;

    private void Awake()
    {
        this.imageView = this.GetComponent<RawImage>();
        if (this.imageView == null)
        {
            this.imageView = this.gameObject.AddComponent<RawImage>();
        }
    }

    /// <summary>
    /// Display content as QR Code
    /// </summary>
    /// <param name="content">The given content to be display as QR Code</param>
    public void display(string content)
    {
        Texture2D qrcodeTexture = convertTextToQRCode(content);
        if (this.imageView != null)
        {
            this.imageView.texture = qrcodeTexture;
        }
    }

    private Texture2D convertTextToQRCode(string text)
    {
        var encoded = new Texture2D(256, 256);
        var color32 = ConvertBitMatrixToColor32(Encode(text, encoded.width, encoded.height));
        encoded.SetPixels32(color32);
        encoded.Apply();
        return encoded;
    }

    private static ZXing.Common.BitMatrix Encode(string textForEncoding, int width, int height)
    {
        var writer = new MultiFormatWriter();
        return writer.encode(
            textForEncoding,
            BarcodeFormat.QR_CODE,
            width,
            height,
            new QrCodeEncodingOptions
            {
                Margin = 1
            }.Hints
        );
    }

    private static Color32[] ConvertBitMatrixToColor32(ZXing.Common.BitMatrix bitMatrix)
    {
        int width = bitMatrix.Width;
        int height = bitMatrix.Height;
        Color32 black = new Color32(0, 0, 0, 255);
        Color32 white = new Color32(255, 255, 255, 255);
        Color32[] colorArray = new Color32[width * height];
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                colorArray[y * width + x] = bitMatrix[x, height - y - 1] ? black : white;
            }
        }
        return colorArray;
    }


}