using System.Collections;
using System.Collections.Generic;
using Microsoft.CSharp;
using UnityEngine;

public class ConfigPanelGUI : MonoBehaviour
{

    private Vector2 scrollViewVector = Vector2.zero;

    private string textFieldString;

    private List<string> configKeys = new List<string>();
    private Dictionary<string, string> configValues = new Dictionary<string, string>();

    private RectCursor cursor = new RectCursor();

    private Config config = null;

    private bool isShow = false;

    static private GameObject configGUI = null;
    static private ConfigPanelGUI gui = null;

    static private ConfigPanelGUI shared()
    {
        if (configGUI == null)
        {
            configGUI = new GameObject("ConfigPanelGUIGameObject-DontDestroyOnLoad");
            gui = configGUI.AddComponent<ConfigPanelGUI>();
        }
        return gui;
    }

    static public void show()
    {
        ConfigPanelGUI.shared()._show();
    }

    static public void close()
    {
        ConfigPanelGUI.shared()._close();
    }


    void OnGUI()
    {
        if (!isShow)
        {
            return;
        }

        GUI.Box(cursor.rect(0, 0, 960, 540), "");


        // Begin the ScrollView
        float scrollViewHeight = 540 * 3.0f;
        scrollViewVector = GUI.BeginScrollView(cursor.rect(0, 0, 960, 540), scrollViewVector, cursor.rect(10, 0, 900, scrollViewHeight));

        cursor.reset(900.0f, 35.0f);

        GUI.Label(cursor.next(), "Runtime configuration and debug panel");

        configKeys.ForEach(key =>
        {
            renderInput(key);
        });

        if (GUI.Button(cursor.next(1, 50), "Save and close"))
        {
            isShow = false;
            saveConfig();
        }

        if (GUI.Button(cursor.next(1, 30), "Close without saving"))
        {
            isShow = false;
        }


        if (GUI.Button(cursor.next(1, 30), "Reset default, save and close"))
        {
            loadConfig(true);
            isShow = false;
        }

        // End the ScrollView
        GUI.EndScrollView();
    }

    private void _show()
    {
        if (!isShow)
        {
            loadConfig();
            isShow = true;
        }
    }
    private void _close()
    {
        isShow = false;
    }

    private void loadConfig(bool isForceLoadFromResources = false)
    {
        if (isForceLoadFromResources)
        {
            ConfigManager.ReloadConfig();
        }
        config = ConfigManager.GetConfig();
        foreach (ConfigItem item in config.configItems)
        {
            if (configKeys.IndexOf(item.key) < 0)
            {
                configKeys.Add(item.key);
            }

            if (!configValues.ContainsKey(item.key))
            {
                configValues.Add(item.key, item.value);
            }
            else
            {
                configValues[item.key] = item.value;
            }
        }
    }

    private void saveConfig()
    {
        configKeys.ForEach(key =>
        {
            for (int i = 0; i < config.configItems.Length; i++)
            {
                if (config.configItems[i].key == key)
                {
                    config.configItems[i].value = configValues.ContainsKey(key) ? configValues[key] : "";
                }
            }
        });

        ConfigManager.SaveConfig(config);
    }

    private void renderInput(string key)
    {
        if (!configValues.ContainsKey(key))
        {
            configValues.Add(key, "");
        }

        GUI.Label(cursor.next(), key + ":");
        configValues[key] = GUI.TextField(cursor.next(1.0f, 0.0f), configValues[key]);
    }

}

class RectCursor
{
    const float GUI_DESIGN_WIDTH = 960.0f;

    private float curY, width, height;
    public void reset(float w, float h)
    {
        curY = 0.0f;
        width = w;
        height = h;
    }

    public Rect rect(float x, float y, float w, float h)
    {
        float scale = Screen.width / GUI_DESIGN_WIDTH;
        return new Rect(x * scale, y * scale, w * scale, h * scale);
    }

    public Rect next(float scaleWidth = 1.0f, float marginTop = 10.0f)
    {
        float scale = Screen.width / GUI_DESIGN_WIDTH;
        curY += marginTop;
        Rect rect = new Rect(30 * scale, curY * scale, width * scale * scaleWidth, height * scale);
        curY += rect.height;
        return rect;
    }

}