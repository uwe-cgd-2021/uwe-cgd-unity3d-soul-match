using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingItem : MonoBehaviour
{
    /// <summary>
    /// To create a new game object with the falling item component according to the given player index and falling item type
    /// </summary>
    /// <param name="playerIndex">The player index from 0 to 3 only.</param>
    /// <param name="playertypeIndex">The type of the item.</param>
    /// <param name="parent">The parent the item should attach to</param>
    /// <return>Returns a fallingItem component</return>
    static public FallingItem createFallingItem(int playerIndex, FallingItemType type, GameObject parent = null)
    {
        GameObject itemObject = new GameObject("falling-item-" + type + "-" + playerIndex);
        if (parent != null)
        {
            itemObject.transform.SetParent(parent.transform);
            itemObject.transform.localEulerAngles = Vector3.zero;
        }

        FallingItem fallingItem = itemObject.AddComponent<FallingItem>();
        fallingItem.setData(playerIndex, type);
        return fallingItem;
    }

    /// <summary>
    /// To pair two items as a record.
    /// </summary>
    /// <param name="main">The item as the main item</param>
    /// <param name="side">The item as the side item</param>
    static public void pairFallingItems(FallingItem main, FallingItem side)
    {
        main.setPartner(side, true);
        side.setPartner(main, false);
    }



    private enum COLOUR_TYPE
    {
        COLOR_MAIN,
        COLOR_SIDE
    }

    private static Dictionary<int, Dictionary<COLOUR_TYPE, Color>> playersColours = new Dictionary<int, Dictionary<COLOUR_TYPE, Color>>{
        {0, new Dictionary<COLOUR_TYPE, Color>{
            {COLOUR_TYPE.COLOR_MAIN, new Color(48f/255f, 255f/255f, 0f/255f)},
            {COLOUR_TYPE.COLOR_SIDE, new Color(82f/255f, 132f/255f, 71f/255f)}}
        },
        {
            1, new Dictionary<COLOUR_TYPE, Color>{
                {COLOUR_TYPE.COLOR_MAIN, new Color(255f/255f, 68f/255f, 0f/255f)},
                {COLOUR_TYPE.COLOR_SIDE, new Color(255f/255f, 141f/255f, 99f/255f)}}
        },
        {
            2, new Dictionary<COLOUR_TYPE, Color>{
                {COLOUR_TYPE.COLOR_MAIN, new Color(0f/255f, 20f/255f, 226f/255f)},
                {COLOUR_TYPE.COLOR_SIDE, new Color(95f/255f, 134f/255f, 255f/255f)}}
        },
        {
            3, new Dictionary<COLOUR_TYPE, Color>{
                {COLOUR_TYPE.COLOR_MAIN, new Color(255f/255f, 0f/255f, 221f/255f)},
                {COLOUR_TYPE.COLOR_SIDE, new Color(206f/255f, 92f/255f, 169f/255f)}}
        }
    };


    private int playerIndex;
    private FallingItemType type;
    private GameObject designObj;

    private FallingItem partner = null;
    private bool isMain = false;
    private bool isLock = false;


    public int refColIndex, refRowIndex = 0;

    private Dictionary<FallingItemType, ParticlesType> particleFor = new Dictionary<FallingItemType, ParticlesType>
    {
        {FallingItemType.VIDEO_GAME, ParticlesType.FALLINGITEM_STAR },
        {FallingItemType.TRAVEL, ParticlesType.FALLINGITEM_STAR },
        {FallingItemType.FOODIE, ParticlesType.FALLINGITEM_STAR },
        {FallingItemType.READING, ParticlesType.FALLINGITEM_STAR },
        {FallingItemType.MOVIE, ParticlesType.FALLINGITEM_STAR },
        {FallingItemType.MUSIC, ParticlesType.FALLINGITEM_STAR },
        {FallingItemType.SPORT, ParticlesType.FALLINGITEM_STAR },
        {FallingItemType.ACTS_OF_SERVICE, ParticlesType.FALLINGITEM_HEART },
        {FallingItemType.WORDS_OF_AFFIRMATION, ParticlesType.FALLINGITEM_HEART },
        {FallingItemType.PHYSICAL_TOUCH, ParticlesType.FALLINGITEM_HEART },
        {FallingItemType.GIFT_GIVING, ParticlesType.FALLINGITEM_HEART },
        {FallingItemType.QUALITY_OF_TIME, ParticlesType.FALLINGITEM_HEART }
    };

    /// <summary>
    /// Set position of the item
    /// </summary>
    /// <param name="position">The position</param>
    /// <param name="isAnimated">With animation if true</param>
    public void setPosition(Vector3 position, bool isAnimated = true)
    {
        if (isAnimated)
        {
            AnimateTransform.animateLocalPosition(this.gameObject, position);
        }
        else
        {
            this.transform.localPosition = position;
        }
    }

    /// <summary>
    /// Check if the given item has the same falling item type
    /// </summary>
    /// <param name="item">The given item to be compared</param>
    /// <return>Returns a boolean, true if it's matched</return>
    public bool isMatch(FallingItem item)
    {
        return item != null && item != this && item.type == this.type;
    }

    public void lockItem()
    {
        if (isLock)
        {
            return;
        }
        isLock = true;
        unpairPartner();
    }


    /// <summary>
    /// Remove the item. Also to unregister from the partner item.
    /// </summary>
    public void remove()
    {
        // Scale down the design object
        StartCoroutine(AnimateTransform.doActionAfterDelay(() =>
        {
            // Add particle
            if (particleFor.ContainsKey(type))
            {
                GameObject particle = ParticleHelper.playParticle(particleFor[type], 0.45f);
                particle.transform.parent = gameObject.transform;
                particle.transform.localPosition = Vector3.zero;
            }
            // Scale down
            AnimateTransform.animateLocalScale(designObj, Vector3.zero, 0.25f);
        }, 0.25f));

        //Unpair partner
        unpairPartner();

        //Destroy itself after 0.6s
        Destroy(this.gameObject, 0.6f);
    }

    /// <summary>
    /// set the colour of the box behind the falling item
    /// </summary>
    /// <param name="r">the r of rgb (0-255)</param>
    /// <param name="g">the g of rgb (0-255)</param>
    /// <param name="b">the b of rgb (0-255)</param>
    public void setColour(float r, float g, float b)
    {
        setColour(new Color(r / 255f, g / 255f, b / 255f));
    }

    /// <summary>
    /// set the colour of the box behind the falling item
    /// </summary>
    /// <param name="color">The colour</param> 
    public void setColour(Color color)
    {
        Transform box = designObj.transform.Find("01_playerCube_Loc");
        if (box != null)
        {
            MeshRenderer[] meshRenderers = box.GetComponentsInChildren<MeshRenderer>();
            foreach (MeshRenderer meshRenderer in meshRenderers)
            {
                meshRenderer.material.color = color;
            }
        }
    }


    /// <summary>
    /// Get the player index
    /// </summary>
    /// <return>Returns int as player index</return>
    public int getPlayerIndex()
    {
        return this.playerIndex;
    }

    public FallingItem getPartner()
    {
        return partner;
    }
    public bool getIsMain()
    {
        return isMain;
    }

    public bool getIsLock()
    {
        return isLock;
    }

    public FallingItemType GetItemType()
    {
        return type;
    }

    private void unpairPartner()
    {
        if (partner != null)
        {
            partner.setPartner(null, true);
        }
        setPartner(null, false);
    }

    private void setPartner(FallingItem partner, bool isMain)
    {
        this.partner = partner;
        this.isMain = isMain;
        updateBoxColour();
    }

    private void updateBoxColour()
    {
        if (isLock)
        {
            setColour(220f, 220f, 220f);
        }
        else
        {
            if (!playersColours.ContainsKey(playerIndex))
            {
                Debug.LogError("Player index " + playerIndex + " is not defined");
                return;
            }
            setColour(playersColours[playerIndex][isMain ? COLOUR_TYPE.COLOR_MAIN : COLOUR_TYPE.COLOR_SIDE]);
        }
    }

    private void setData(int playerIndex, FallingItemType type)
    {
        this.playerIndex = playerIndex;
        this.type = type;

        designObj = FallingItemHelper.getFallingItemPrefab(playerIndex, type);
        designObj.transform.parent = this.transform;
        designObj.transform.localEulerAngles = Vector3.zero;

        updateBoxColour();
    }

}
