using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class FallingItemWall : MonoBehaviour
{

    private const int ROW_BUFFER = 5;

    private int totalRowCount = 0;
    private int totalColumnCount = 0;


    private FallingItemCategory category;

    private List<List<FallingItem>> itemColumns = null;

    private Dictionary<int, FallingItemWallPlayerProfile> players;

    private bool isRunning = false;
    private bool isGameOver = false;

    private float fallingTargetDuration = 0.5f;
    private float fallingCurrentDuration = 0.0f;

    private UnityEvent<FallingItemCategory, FallingItem> eventOnGamover = new UnityEvent<FallingItemCategory, FallingItem>();

    private UnityEvent<FallingItemCategory, FallingItemType> eventOnItemMatched = new UnityEvent<FallingItemCategory, FallingItemType>();

    private UnityEvent<int> eventOnPlayerShouldCreateItem = new UnityEvent<int>();


    public static FallingItemWall createFallingItemWall(GameObject parent)
    {
        GameObject wallObj = new GameObject("wall");
        wallObj.transform.parent = parent.transform;
        return wallObj.AddComponent<FallingItemWall>();
    }

    private static int getOffsetColumnIndex(int columnIndex, FallingItemWallPlayerProfile.Direction direction)
    {
        int offset = direction == FallingItemWallPlayerProfile.Direction.LEFT ? -1 : (direction == FallingItemWallPlayerProfile.Direction.RIGHT ? 1 : 0);
        return columnIndex + offset;
    }

    private static int getOffsetRowIndex(int rowIndex, FallingItemWallPlayerProfile.Direction direction)
    {
        int offset = direction == FallingItemWallPlayerProfile.Direction.UP ? +1 : (direction == FallingItemWallPlayerProfile.Direction.DOWN ? -1 : 0);
        return rowIndex + offset;
    }


    private void Update()
    {
        if (!isRunning || isGameOver)
        {
            return;
        }

        if (itemColumns == null)
        {
            Debug.LogError("itemColumns is null. There should be some logic issue.");
            return;
        }

        //Check end game
        for (int i = 0; i < itemColumns.Count; i++)
        {
            FallingItem item = itemColumns[i][itemColumns[i].Count - ROW_BUFFER - 1];
            if (item != null && item.getIsLock())
            {
                isGameOver = true;
                isRunning = false;
                eventOnGamover.Invoke(category, item);
                return;
            }
        }

        // Moving main items according to the player column and its row index
        for (int i = 0; i < itemColumns.Count; i++)
        {
            for (int j = 0; j < itemColumns[i].Count - 1; j++)
            {
                FallingItem item = getItem(i, j);
                if (item != null && item.getIsMain() && j > 0)
                {
                    int playerIndex = item.getPlayerIndex();
                    int mainItemColumnIndex = this.players[playerIndex].columnIndex;
                    int mainItemRowIndex = j;
                    int sideItemColumnIndex = FallingItemWall.getOffsetColumnIndex(mainItemColumnIndex, this.players[playerIndex].direction);
                    int sideItemRowIndex = FallingItemWall.getOffsetRowIndex(mainItemRowIndex, this.players[playerIndex].direction);


                    FallingItem partnerItem = item.getPartner();
                    if (partnerItem != null)
                    {
                        moveItem(partnerItem.refColIndex, partnerItem.refRowIndex, sideItemColumnIndex, sideItemRowIndex);
                    }

                    if (isAvailable(mainItemColumnIndex, mainItemRowIndex))
                    {
                        moveItem(i, j, mainItemColumnIndex, mainItemRowIndex);
                    }
                }
            }
        }

        // Move partner items according to the main items
        for (int i = 0; i < itemColumns.Count; i++)
        {
            for (int j = 0; j < itemColumns[i].Count - 1; j++)
            {
                FallingItem item = getItem(i, j);
                if (item != null && !item.getIsMain() && j > 0)
                {
                    FallingItem partnerItem = item.getPartner();

                    if (partnerItem != null)
                    {
                        int playerIndex = item.getPlayerIndex();

                        int mainItemColumnIndex = partnerItem.refColIndex;
                        int mainItemRowIndex = partnerItem.refRowIndex;
                        int sideItemColumnIndex = FallingItemWall.getOffsetColumnIndex(mainItemColumnIndex, this.players[playerIndex].direction);
                        int sideItemRowIndex = FallingItemWall.getOffsetRowIndex(mainItemRowIndex, this.players[playerIndex].direction);
                        moveItem(i, j, sideItemColumnIndex, sideItemRowIndex);
                    }
                }
            }
        }


        // Falling
        fallingCurrentDuration += Time.unscaledDeltaTime;
        if (fallingCurrentDuration >= fallingTargetDuration)
        {
            fallingCurrentDuration = 0.0f;

            for (int i = 0; i < itemColumns.Count; i++)
            {
                for (int j = 0; j < itemColumns[i].Count - 1; j++)
                {
                    moveItem(i, j + 1, i, j);
                }
            }
        }

        // Lock items from user control
        for (int i = 0; i < itemColumns.Count; i++)
        {
            for (int j = 0; j < itemColumns[i].Count; j++)
            {
                FallingItem item = itemColumns[i][j];
                if (item != null && !item.getIsLock())
                {
                    // TODO: check all avaible
                    bool isLock = j == 0;
                    bool hasAvailabelGap = false;
                    for (int k = (j - 1); k >= 0; k--)
                    {
                        if (isAvailable(i, k))
                        {
                            hasAvailabelGap = true;
                        }
                    }
                    isLock = hasAvailabelGap && !isLock ? false : true;

                    if (isLock)
                    {
                        item.lockItem();
                    }
                }
            }
        }


        //Update all items position according to the data
        for (int i = 0; i < itemColumns.Count; i++)
        {
            for (int j = 0; j < itemColumns[i].Count; j++)
            {
                FallingItem item = itemColumns[i][j];
                if (item != null)
                {
                    item.setPosition(getItemPosition(i, j));
                }
            }
        }

        // Check connected item
        for (int i = 0; i < itemColumns.Count; i++)
        {
            for (int j = 0; j < itemColumns[i].Count; j++)
            {
                FallingItem item = itemColumns[i][j];
                if (item != null && item.getIsLock())
                {
                    List<FallingItem> connectedItems = getConnectedItems(item);
                    if (connectedItems.Count > 0)
                    {
                        StartCoroutine(AnimateTransform.doActionAfterDelay(() =>
                        {
                            PlaySound.playAudio(SoundEffectAndMusic.PUZZLE_ELIMINATE, gameObject);
                        }, 0.1f));


                        eventOnItemMatched.Invoke(category, item.GetItemType());
                        connectedItems.ForEach((FallingItem connectedItem) =>
                        {
                            connectedItem.remove();
                            setItem(connectedItem.refColIndex, connectedItem.refRowIndex, null);
                        });
                    }
                }
            }
        }

        // Should create 
        foreach (int playerIndex in this.players.Keys)
        {
            if (shouldCreateItem(playerIndex))
            {
                eventOnPlayerShouldCreateItem.Invoke(playerIndex);
            }
        }
    }

    private List<FallingItem> getConnectedItems(FallingItem item, List<FallingItem> checkedItems = null)
    {
        List<FallingItem> connectedItems = checkedItems != null ? checkedItems : new List<FallingItem>();
        List<FallingItem> futherCheckItems = new List<FallingItem>();

        if (item != null && item.getIsLock())
        {
            int i = item.refColIndex;
            int j = item.refRowIndex;
            List<FallingItem> checkItems = new List<FallingItem>{
                        getItem(i+1, j),
                        getItem(i-1, j),
                        getItem(i, j+1),
                        getItem(i, j-1)
                    };

            checkItems.ForEach((FallingItem checkItem) =>
            {
                if (checkItem != null && checkItem.getIsLock() && item.isMatch(checkItem) && !connectedItems.Contains(checkItem))
                {
                    if (!connectedItems.Contains(item))
                    {
                        connectedItems.Add(item);
                    }
                    if (!connectedItems.Contains(checkItem))
                    {
                        connectedItems.Add(checkItem);
                        if (!futherCheckItems.Contains(checkItem))
                        {
                            futherCheckItems.Add(checkItem);
                        }
                    }
                }
            });
        }

        //Find the other connected items from the items connected to the item
        if (futherCheckItems.Count > 0)
        {
            futherCheckItems.ForEach((FallingItem newItem) =>
            {
                getConnectedItems(newItem, connectedItems).ForEach((FallingItem newConnectedItem) =>
                  {
                      if (newConnectedItem != null && !connectedItems.Contains(newConnectedItem))
                      {
                          connectedItems.Add(newConnectedItem);
                      }
                  });
            });
        }


        return connectedItems;
    }

    private void moveItem(int fromColumnIndex, int formRowIndex, int toColumnIndex, int toRowIndex)
    {
        if (isAvailable(toColumnIndex, toRowIndex))
        {
            FallingItem aboveItem = getItem(fromColumnIndex, formRowIndex);
            setItem(toColumnIndex, toRowIndex, aboveItem);
            setItem(fromColumnIndex, formRowIndex, null);
        }
    }

    public void start(FallingItemCategory category, int columnCount, int rowCount, int[] playerIndexes)
    {
        // Reset
        if (itemColumns != null)
        {
            itemColumns.ForEach((List<FallingItem> items) =>
            {
                items.ForEach((FallingItem item) =>
                {
                    if (item != null)
                    {
                        Destroy(item.gameObject);
                    }
                });
            });
            itemColumns = null;
        }

        if (players != null)
        {
            players = null;
        }

        //Set row and columns count
        totalRowCount = rowCount + ROW_BUFFER;
        totalColumnCount = columnCount;

        //Set category
        this.category = category;

        // Initialise lists for items
        itemColumns = new List<List<FallingItem>>();
        for (int i = 0; i < totalColumnCount; i++)
        {
            List<FallingItem> items = new List<FallingItem>();
            for (int j = 0; j < totalRowCount; j++)
            {
                items.Add(null);
            }
            itemColumns.Add(items);
        }

        //Set the player indexes
        this.players = new Dictionary<int, FallingItemWallPlayerProfile>();
        foreach (int playerIndex in playerIndexes)
        {
            this.players.Add(playerIndex, new FallingItemWallPlayerProfile(playerIndex));
        }

        //Start running update
        isRunning = true;
        isGameOver = false;
    }

    public void stopForGameover()
    {
        isRunning = false;
        isGameOver = true;
    }

    public void updatePlayerProfile(int playerIndex, int columnIndex, FallingItemWallPlayerProfile.Direction direction)
    {
        if (this.players.ContainsKey(playerIndex))
        {
            this.players[playerIndex].columnIndex = Mathf.Max(1, Mathf.Min(totalColumnCount - 2, columnIndex));
            this.players[playerIndex].direction = direction;
        }
        else
        {
            Debug.LogError("Player doesn't exist.");
        }
    }
    public void updatePlayerProfile(FallingItemWallPlayerProfile player)
    {
        updatePlayerProfile(player.playerIndex, player.columnIndex, player.direction);
    }

    public void registerOnPlayerShouldCreateItem(UnityAction<int> onPlayerShouldCreateItem, bool isRemoveOtherListners = false)
    {
        if (onPlayerShouldCreateItem != null)
        {
            if (isRemoveOtherListners)
            {
                eventOnPlayerShouldCreateItem.RemoveAllListeners();
            }
            eventOnPlayerShouldCreateItem.AddListener(onPlayerShouldCreateItem);
        }
    }

    public void registerOnGameover(UnityAction<FallingItemCategory, FallingItem> onGameover, bool isRemoveOtherListners = false)
    {
        if (onGameover != null)
        {
            if (isRemoveOtherListners)
            {
                eventOnGamover.RemoveAllListeners();
            }
            eventOnGamover.AddListener(onGameover);
        }
    }

    public void registerOnItemMatched(UnityAction<FallingItemCategory, FallingItemType> onItemMatched, bool isRemoveOtherListners = false)
    {
        if (onItemMatched != null)
        {
            if (isRemoveOtherListners)
            {
                eventOnItemMatched.RemoveAllListeners();
            }
            eventOnItemMatched.AddListener(onItemMatched);
        }
    }

    public bool shouldCreateItem(int playerIndex)
    {
        if (!isAllowCreateItem(playerIndex))
        {
            return false;
        }

        bool isControllingItem = false;
        itemColumns.ForEach((List<FallingItem> items) =>
        {
            items.ForEach((FallingItem item) =>
            {
                if (item != null && !item.getIsLock() && item.getPlayerIndex() == playerIndex)
                {
                    isControllingItem = true;
                }
            });
        });
        return !isControllingItem;
    }

    private bool isAllowCreateItem(int playerIndex)
    {
        if (!isGameOver && this.players.ContainsKey(playerIndex))
        {
            int mainItemColumnIndex = this.players[playerIndex].columnIndex;
            int mainItemRowIndex = totalRowCount - 2;
            int sideItemColumnIndex = FallingItemWall.getOffsetColumnIndex(mainItemColumnIndex, this.players[playerIndex].direction);
            int sideItemRowIndex = FallingItemWall.getOffsetRowIndex(mainItemRowIndex, this.players[playerIndex].direction);
            return (isAvailable(mainItemColumnIndex, mainItemRowIndex) && isAvailable(sideItemColumnIndex, sideItemRowIndex));
        }
        return false;
    }

    public bool createItems(int playerIndex)
    {
        if (!isAllowCreateItem(playerIndex))
        {
            return false;
        }

        bool isSuccess = false;


        int mainItemColumnIndex = this.players[playerIndex].columnIndex;
        int mainItemRowIndex = totalRowCount - 2;
        int sideItemColumnIndex = FallingItemWall.getOffsetColumnIndex(mainItemColumnIndex, this.players[playerIndex].direction);
        int sideItemRowIndex = FallingItemWall.getOffsetRowIndex(mainItemRowIndex, this.players[playerIndex].direction);

        List<FallingItem> items = new List<FallingItem>(2);
        foreach (FallingItemType type in FallingItemHelper.getRandomTypes(category))
        {
            FallingItem item = FallingItem.createFallingItem(playerIndex, type, this.gameObject);
            item.setPosition(getItemPosition(players[playerIndex].columnIndex, totalRowCount), false);
            items.Add(item);
        }
        FallingItem.pairFallingItems(items[0], items[1]);
        setItem(mainItemColumnIndex, mainItemRowIndex, items[0]);
        setItem(sideItemColumnIndex, sideItemRowIndex, items[1]);

        isSuccess = true;

        logColumns();

        return isSuccess;
    }

    public int calculateColumnIndex(float percentage)
    {
        return Mathf.RoundToInt((itemColumns.Count - 2) * percentage) + 1;
    }

    public FallingItemWallPlayerProfile.Direction calculateDirection(float angle)
    {
        float fixedAngle = ((angle % 360) + 360) % 360;


        if (fixedAngle >= 45f && fixedAngle < 135f)
        {
            return FallingItemWallPlayerProfile.Direction.UP;
        }

        if (fixedAngle >= 135f && fixedAngle < 225f)
        {
            return FallingItemWallPlayerProfile.Direction.LEFT;
        }

        if (fixedAngle >= 225f && fixedAngle < 315f)
        {
            return FallingItemWallPlayerProfile.Direction.DOWN;
        }

        return FallingItemWallPlayerProfile.Direction.RIGHT;
    }

    private Vector3 getItemPosition(int columnIndex, int rowIndex)
    {
        return new Vector3(columnIndex * 2f, rowIndex * 2f, 0);
    }

    private void setItem(int columnIndex, int rowIndex, FallingItem item)
    {
        if (itemColumns != null && columnIndex >= 0 && rowIndex >= 0)
        {
            if (columnIndex < itemColumns.Count && rowIndex < itemColumns[columnIndex].Count)
            {
                itemColumns[columnIndex][rowIndex] = item;
                if (item != null)
                {
                    item.refColIndex = columnIndex;
                    item.refRowIndex = rowIndex;
                }
            }
        }
    }

    private FallingItem getItem(int columnIndex, int rowIndex)
    {
        if (itemColumns != null && columnIndex >= 0 && rowIndex >= 0 && columnIndex < itemColumns.Count && rowIndex < itemColumns[columnIndex].Count)
        {
            if (!isAvailable(columnIndex, rowIndex))
            {
                return itemColumns[columnIndex][rowIndex];
            }
        }
        return null;
    }

    private bool isAvailable(int columnIndex, int rowIndex)
    {
        if (itemColumns != null && columnIndex >= 0 && rowIndex >= 0)
        {
            if (columnIndex < itemColumns.Count && rowIndex < itemColumns[columnIndex].Count)
            {
                return itemColumns[columnIndex][rowIndex] == null;
            }
        }
        return false;
    }



    private void logColumns()
    {
        if (itemColumns != null)
        {
            List<string> rowStr = new List<string>();
            for (int col = 0; col < itemColumns.Count; col++)
            {
                for (int row = 0; row < itemColumns[col].Count; row++)
                {
                    if (row >= rowStr.Count)
                    {
                        rowStr.Add("");
                    }
                    rowStr[row] += (itemColumns[col][itemColumns[col].Count - 1 - row] != null ? " 1 " : " 0 ");
                }
            }

            Debug.Log("===============================");
            rowStr.ForEach((string str) =>
            {
                Debug.Log(str);
            });
            Debug.Log("===============================");
        }
    }

}
