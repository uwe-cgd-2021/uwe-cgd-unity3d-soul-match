using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GamplayManager : MonoBehaviour
{
    private enum WallType
    {
        FRONT,
        RIGHT
    }

    private float percentageOfAxisToReady = 0.5f;
    private bool isRunning = false;
    private float fRunningTime = 0.0f;
    private Dictionary<WallType, FallingItemWall> walls;
    private UnityEvent eventOnGameover = new UnityEvent();
    private UnityEvent<FallingItemCategory, FallingItemType> eventOnItemMatched = new UnityEvent<FallingItemCategory, FallingItemType>();

    private List<Dictionary<WallType, FallingItemWallPlayerProfile>> players = null;


    private void Start()
    {
        //Init players
        for (int i = 0; i < 4; i++)
        {
            PlayerTrackerManager.getPlayer(i);
        }
    }


    /// <summary>
    /// Register a callback for gameover. Don't register more than one. 
    /// You can also consider to use `isRemoveOtherListners` according to the situations.
    /// </summary>
    /// <param name="onGameover">Callback action</param>
    /// <param name="isRemoveOtherListners">True to remove all other listeners before adding. Default false.</param>
    public void registerOnGameover(UnityAction onGameover, bool isRemoveOtherListners = false)
    {
        if (onGameover != null)
        {
            if (isRemoveOtherListners)
            {
                eventOnGameover.RemoveAllListeners();
            }
            eventOnGameover.AddListener(onGameover);
        }
    }

    /// <summary>
    /// Register a callback for item matched. Don't register more than one. 
    /// You can also consider to use `isRemoveOtherListners` according to the situations.
    /// </summary>
    /// <param name="onGameover">Callback action</param>
    /// <param name="isRemoveOtherListners">True to remove all other listeners before adding. Default false.</param>
    public void registerOnItemMatched(UnityAction<FallingItemCategory, FallingItemType> onItemMatched, bool isRemoveOtherListners = false)
    {
        if (onItemMatched != null)
        {
            if (isRemoveOtherListners)
            {
                eventOnItemMatched.RemoveAllListeners();
            }
            eventOnItemMatched.AddListener(onItemMatched);
        }
    }

    /// <summary>
    /// Stop the game. There is no way to restart the game for now.
    //  This function is designed for the controller to call when the game is win after reaching a specific score.
    /// </summary>
    public void stop()
    {
        isRunning = false;

        foreach (WallType wallType in walls.Keys)
        {
            if (walls[wallType] != null)
            {
                walls[wallType].stopForGameover();
            }
        }
    }


    /// <summary>
    /// Start a new game. To reset all items and create two new walls for the new game.
    /// </summary>
    public void startGame()
    {
        //Reset
        if (walls != null)
        {
            foreach (WallType type in walls.Keys)
            {
                FallingItemWall wall = walls[type];
                if (wall != null)
                {
                    wall.stopForGameover();
                }
                Destroy(wall.gameObject);
                wall = null;
            }
        }

        //Get Count from config
        int columnCount = ConfigManager.GetInt(ConfigKey.COLUMNS_COUNT);
        int rowCount = ConfigManager.GetInt(ConfigKey.ROWS_COUNT);

        //Init players
        List<int> playerIndexes = GetReadyPlayersIndex();

        players = new List<Dictionary<WallType, FallingItemWallPlayerProfile>>();
        for (int i = 0; i < playerIndexes.Count; i++)
        {
            players.Add(new Dictionary<WallType, FallingItemWallPlayerProfile>{
               { WallType.FRONT, new FallingItemWallPlayerProfile(playerIndexes[i])},
               { WallType.RIGHT, new FallingItemWallPlayerProfile(playerIndexes[i])}
            });
        }

        //Init two walls
        walls = new Dictionary<WallType, FallingItemWall>(2);
        walls.Add(WallType.FRONT, FallingItemWall.createFallingItemWall(this.gameObject));
        walls.Add(WallType.RIGHT, FallingItemWall.createFallingItemWall(this.gameObject));

        //Update position
        walls[WallType.FRONT].gameObject.transform.localPosition = new Vector3(0, 0, columnCount * 2.0f + 2f);

        walls[WallType.RIGHT].gameObject.transform.localPosition = new Vector3(columnCount * 2.0f + 2f, 0, columnCount * 2.0f);
        walls[WallType.RIGHT].gameObject.transform.rotation = Quaternion.Euler(0, 90f, 0f);

        // register events and start
        foreach (WallType type in walls.Keys)
        {
            walls[type].registerOnPlayerShouldCreateItem(onPlayerShouldCreateItem, true);
            walls[type].registerOnGameover(onGameover, true);
            walls[type].registerOnItemMatched(onItemMatched, true);
        }

        // Start game 
        // It'd be better to ensure all of the callbacks have been registered to those two wall before starting, so used a separated foreach here.
        foreach (WallType type in walls.Keys)
        {
            FallingItemCategory category = type == WallType.FRONT ? FallingItemCategory.INTEREST : FallingItemCategory.LOVE_LANGUAGE;
            walls[type].start(category, columnCount, rowCount, playerIndexes.ToArray());
        }

        //Reset running time and set the game is running
        fRunningTime = 0.0f;
        isRunning = true;
    }

    /// <summary>
    /// Return true when the game is running
    /// </summary>
    public bool isGamplayRunning()
    {
        return isRunning;
    }

    /// <summary>
    /// Get running time
    /// </summary>
    /// <return>Returns a float of the running time.</return>
    public float getRunningTime()
    {
        return fRunningTime;
    }

    /// <summary>
    /// Get the index of ready players
    /// </summary>
    /// <return>Returns a list of int about the index of ready players.</return>
    public List<int> GetReadyPlayersIndex()
    {
        List<int> playersIndex = new List<int>();
        for (int i = 0; i < 4; i++)
        {
            PlayerTracker player = PlayerTrackerManager.getPlayer(i);
            if (player != null && !player.transform.position.Equals(Vector3.zero))
            {
                if (player.getPlayareaPosition(PlayerTracker.Axis.Z) <= percentageOfAxisToReady)
                {
                    playersIndex.Add(i);
                }
            }
        }
        return playersIndex;
    }



    private void onItemMatched(FallingItemCategory category, FallingItemType type)
    {
        eventOnItemMatched.Invoke(category, type);
    }

    private void onGameover(FallingItemCategory category, FallingItem item)
    {
        //Stop the game
        stop();

        //Invoke event after 0.5 seconds
        StartCoroutine(AnimateTransform.doActionAfterDelay(() =>
        {
            eventOnGameover.Invoke();
        }, 0.5f));

    }

    private void onPlayerShouldCreateItem(int playerIndex)
    {
        if (walls != null && walls.ContainsKey(WallType.FRONT) && walls.ContainsKey(WallType.RIGHT))
        {
            if (walls[WallType.FRONT].shouldCreateItem(playerIndex) && walls[WallType.RIGHT].shouldCreateItem(playerIndex))
            {
                walls[WallType.FRONT].createItems(playerIndex);
                walls[WallType.RIGHT].createItems(playerIndex);
            }
        }
    }


    private void Update()
    {
        if (!isRunning)
        {
            return;
        }
        fRunningTime += Time.unscaledDeltaTime;


        if (players != null && walls != null)
        {
            players.ForEach((Dictionary<WallType, FallingItemWallPlayerProfile> profiles) =>
            {
                foreach (WallType type in profiles.Keys)
                {
                    FallingItemWallPlayerProfile player = profiles[type];
                    PlayerTracker tracker = PlayerTrackerManager.getPlayer(player.playerIndex);
                    if (tracker != null && walls.ContainsKey(type))
                    {
                        //Update the profile according to the data of tracker
                        PlayerTracker.Axis positionAxis = type == WallType.FRONT ? PlayerTracker.Axis.X : PlayerTracker.Axis.Z;
                        player.columnIndex = walls[type].calculateColumnIndex(tracker.getPlayareaPosition(positionAxis));
                        player.direction = walls[type].calculateDirection(tracker.getAngle(type == WallType.FRONT ? PlayerTracker.Axis.Z : PlayerTracker.Axis.X));

                        //Update wall
                        walls[type].updatePlayerProfile(player);
                    }
                }
            });
        }
    }


}
