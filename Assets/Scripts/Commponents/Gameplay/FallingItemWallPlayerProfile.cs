using System.Collections;
using System.Collections.Generic;

public class FallingItemWallPlayerProfile
{
    public enum Direction
    {
        UP, LEFT, RIGHT, DOWN
    }

    public int playerIndex = -1;
    public int columnIndex = 1;
    public Direction direction = Direction.RIGHT;

    public FallingItemWallPlayerProfile(int playerIndex)
    {
        this.playerIndex = playerIndex;

    }
}