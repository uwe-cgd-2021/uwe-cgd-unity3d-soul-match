using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CoupleMissionBar : MonoBehaviour
{
    private int totalMissionCount;

    private CoupleItem coupleItemCounter;

    private List<CoupleItem> couples = new List<CoupleItem>();
    private List<Vector3> CoupleItemPosition = new List<Vector3>();

    private float renderTime = 1.5f;

    public UnityEvent OnMissionFinished = new UnityEvent();

    private CoupleType randomMissionType()
    {
        return CoupleHelper.changeFallingItemTypeToCoupleType(FallingItemHelper.getRandomType(FallingItemCategory.LOVE_LANGUAGE));
    }

    private void renderCouples()
    {
        List<CoupleItem> newCouples = new List<CoupleItem>();
        while (couples.Count < 5 && totalMissionCount > couples.Count)
        {
            CoupleItem item = CoupleItem.createCoupleItem(randomMissionType());
            couples.Add(item);
            newCouples.Add(item);
        }

        for (int x = 0; x < couples.Count; x++)
        {
            bool anim = false;
            anim = !newCouples.Contains(couples[x]);
            if (couples[x] != null)
            {
                couples[x].setPosition(CoupleItemPosition[x], anim);
            }
            else
            {
                couples.RemoveAt(x);
                totalMissionCount--;
            }
        }

        if (totalMissionCount > 5)
        {
            if (coupleItemCounter == null)
            {
                coupleItemCounter = CoupleItem.createCoupleItem(CoupleType.COUNTER, totalMissionCount - 5);
                coupleItemCounter.setPosition(CoupleItemPosition[5], false);
            }
            else
            {
                coupleItemCounter.setCounterNumber(totalMissionCount - 5);
            }
        }
        else if (totalMissionCount <= 5 && coupleItemCounter != null)
        {
            coupleItemCounter.remove();
            coupleItemCounter = null;
        }
    }

    private void resetRenderTime()
    {
        renderTime = 1.5f;
    }

    private void Update()
    {
        renderTime -= Time.unscaledDeltaTime;
        if (renderTime <= 0.0f)
        {
            resetRenderTime();
            renderCouples();
        }
    }


    /// <summary>
    /// Create mission, should be use after a pair of interest matched
    /// </summary>
    public void createMission()
    {
        totalMissionCount++;
        renderCouples();
    }

    /// <summary>
    /// Finish mission, should be use after a pair of love language matched
    /// </summary>
    /// <param name="type">the love language type that is matched</param>
    public void finishMission(FallingItemType type)
    {
        CoupleType coupleType = CoupleHelper.changeFallingItemTypeToCoupleType(type);
        if (coupleType == CoupleType.NULL)
        {
            return;
        }

        for (int x = 0; x < couples.Count; x++)
        {
            if (couples[x] == null)
            {
                couples.RemoveAt(x);
                totalMissionCount--;
            }
            else if (couples[x].typeOfCouple == coupleType)
            {
                couples[x].remove();
                couples.RemoveAt(x);
                totalMissionCount--;
                resetRenderTime();
                OnMissionFinished.Invoke();
                break;
            }
        }
    }


    /// <summary>
    /// set the beginning and ending position of the mission bar
    /// </summary>
    /// <param name="startPoint">The starting position of the couple mission bar, the first mission should appear right here(default Vector3(0.0f,0.0f, 60.0f))</param>
    /// /// <param name="EndPoint">The ending position of the couple mission bar, the counter couple should appear right her(default Vector3(0.0f,0.0f, 10.0f)e</param>
    public void SetCoupleMissionBarPosition(Vector3 startPoint, Vector3 EndPoint)
    {
        CoupleItemPosition = new List<Vector3>();
        CoupleItemPosition.Add(startPoint);
        for (float x = 0.2f; x < 1; x += 0.2f)
        {
            CoupleItemPosition.Add(Vector3.Lerp(startPoint, EndPoint, x));
        }
        CoupleItemPosition.Add(EndPoint);
    }

    /// <summary>
    /// Reset the couple mission bar
    /// </summary>
    public void reset()
    {
        totalMissionCount = 0;

        if (coupleItemCounter != null)
        {
            Destroy(coupleItemCounter.gameObject);
            coupleItemCounter = null;
        }

        if (couples != null)
        {
            couples.ForEach((CoupleItem item) =>
            {
                if (item != null)
                {
                    Destroy(item.gameObject);
                }
            });
        }
        couples = new List<CoupleItem>();
    }
}
