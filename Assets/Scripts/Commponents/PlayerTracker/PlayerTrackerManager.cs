using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class PlayerTrackerManager : MonoBehaviour
{
    static private GameObject ptmObj = null;
    static private PlayerTrackerManager ptm = null;

    static private PlayerTrackerManager shared()
    {
        if (ptmObj == null)
        {
            ptmObj = new GameObject("PlayerTrackerManager-DontDestroyOnLoad");
            DontDestroyOnLoad(ptmObj);
            ptm = ptmObj.AddComponent<PlayerTrackerManager>();
        }
        return ptm;
    }

    /// <summary>
    /// Set Player Index
    /// </summary>
    /// <param name="playerIndex">The player index</param>
    /// <returns>Returns {PlayerTracker|null} the player tracker</returns>
    static public PlayerTracker getPlayer(int playerIndex)
    {
        return PlayerTrackerManager.shared().getPlayerByIndex(playerIndex);
    }


    public int playerCount = 4;
    private List<PlayerTracker> players = new List<PlayerTracker>();
    private List<ConfigKey> playerTrackerConfigKeys = new List<ConfigKey>() {
        ConfigKey.PLAYER_1_TRACKER_INDEX,
        ConfigKey.PLAYER_2_TRACKER_INDEX,
        ConfigKey.PLAYER_3_TRACKER_INDEX,
        ConfigKey.PLAYER_4_TRACKER_INDEX
    };

    private void Awake()
    {
        for (int i = 0; i < playerCount; i++)
        {
            GameObject playerTracker = new GameObject("player-" + (i + 1));
            GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.transform.SetParent(playerTracker.transform, false);
            cube.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
            players.Add(playerTracker.AddComponent<PlayerTracker>());
        }
    }

    private void Start()
    {
        autoPairPlayerTracker();
        updatePlayer();
        ConfigManager.registerConfigSaved(updatePlayer);
    }


    private void updatePlayer()
    {
        for (int i = 0; i < players.Count; i++)
        {
            if (i < playerTrackerConfigKeys.Count)
            {
                players[i].setVRDeviceIndex(ConfigManager.GetInt(playerTrackerConfigKeys[i]));
            }
        }
    }

    private void autoPairPlayerTracker()
    {
        List<uint> trackerDeviceIds = new List<uint>();
        CVRSystem system = OpenVR.System;
        for (uint deviceId = 0; deviceId < 16; deviceId++)
        {
            if (system.GetTrackedDeviceClass(deviceId) == ETrackedDeviceClass.GenericTracker)
            {
                trackerDeviceIds.Add(deviceId);
            }
        }

        if (trackerDeviceIds.Count > 0)
        {
            for (int i = 0; i < Mathf.Min(trackerDeviceIds.Count, players.Count); i++)
            {

                if (i < playerTrackerConfigKeys.Count)
                {
                    ConfigManager.saveInt(playerTrackerConfigKeys[i], (int)trackerDeviceIds[i]);
                }
            }
        }
    }

    private PlayerTracker getPlayerByIndex(int playerIndex)
    {
        return playerIndex < players.Count ? players[playerIndex] : null;
    }

}