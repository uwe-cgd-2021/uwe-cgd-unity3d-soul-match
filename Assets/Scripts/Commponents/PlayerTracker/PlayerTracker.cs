using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR;

public class PlayerTracker : MonoBehaviour
{
    public enum Axis
    {
        Z,
        X
    }

    private SteamVR_RenderModel steamVRRenderModel = null;
    private SteamVR_TrackedObject steamVRTrackedObject = null;

    private int trackerDeviceIndex = -1;

    public bool debug = false;

    private Vector3 playareaLeft, playareaRight;

    private bool isPlayareaFlipX, isPlayareaFlipZ;
    private float playareaPercentageX, playareaPercentageZ;

    private Quaternion lastRotation;
    private float angleX, angleZ = 0.0f;

    private void Awake()
    {
        steamVRRenderModel = this.GetComponentInChildren<SteamVR_RenderModel>();
        steamVRTrackedObject = this.gameObject.AddComponent<SteamVR_TrackedObject>();
    }

    private void Start()
    {
        SteamVR.settings.pauseGameWhenDashboardVisible = false;

        refreshConfig();
        ConfigManager.registerConfigSaved(refreshConfig);

        lastRotation = transform.rotation;
    }

    public float getAngle(Axis axis)
    {
        return axis == Axis.Z ? angleZ : angleX;
    }

    public float getPlayareaPosition(Axis axis)
    {
        return axis == Axis.Z ? playareaPercentageZ : playareaPercentageX;
    }

    private void Update()
    {
        // Playerarea
        playareaPercentageX = Mathf.Min(1.0f, Mathf.Max(0.0f, (this.transform.position.x - playareaLeft.x) / (playareaRight.x - playareaLeft.x)));
        playareaPercentageZ = Mathf.Min(1.0f, Mathf.Max(0.0f, (this.transform.position.z - playareaLeft.z) / (playareaRight.z - playareaLeft.z)));

        if (isPlayareaFlipX)
        {
            playareaPercentageX = 1.0f - playareaPercentageX;
        }

        if (isPlayareaFlipZ)
        {
            playareaPercentageZ = 1.0f - playareaPercentageZ;
        }

        //Rotation
        Quaternion relativeRotation = transform.rotation * Quaternion.Inverse(lastRotation) * transform.rotation * Quaternion.Inverse(transform.rotation);

        float angle;
        Vector3 axis;
        relativeRotation.ToAngleAxis(out angle, out axis);

        angleX += Vector3.Dot(axis, Vector3.right) * angle;
        angleZ += Vector3.Dot(axis, Vector3.forward) * angle;

        lastRotation = transform.rotation;


        if (debug)
        {
            Debug.Log("================================================================");
            Debug.Log("x: " + playareaLeft.x + " " + this.transform.position.x + " " + playareaRight.x);
            Debug.Log("z: " + playareaLeft.z + " " + this.transform.position.z + " " + playareaRight.z);
            Debug.Log("Percentage: " + playareaPercentageX + ", " + playareaPercentageZ);
            Debug.Log("eulerAngles: " + relativeRotation.eulerAngles);
            Debug.Log("angleX: " + angleX);
            Debug.Log("angleZ: " + angleZ);
            Debug.Log("================================================================");
        }
    }

    private void refreshConfig()
    {
        isPlayareaFlipX = ConfigManager.GetInt(ConfigKey.PLAY_AREA_FLIP_X) > 0;
        isPlayareaFlipZ = ConfigManager.GetInt(ConfigKey.PLAY_AREA_FLIP_Z) > 0;
        playareaLeft = ConfigManager.GetVector3(ConfigKey.PLAY_AREA_CORNER_LEFT);
        playareaRight = ConfigManager.GetVector3(ConfigKey.PLAY_AREA_CORNER_RIGHT);
    }


    /// <summary>
    /// Set Device Index
    /// </summary>
    /// <param name="index">The SteamVR device index</param>
    public void setVRDeviceIndex(int index)
    {
        // Set device index
        trackerDeviceIndex = index;

        // Update the model
        if (steamVRRenderModel != null)
        {
            CVRSystem system = OpenVR.System;
            ETrackedPropertyError error = ETrackedPropertyError.TrackedProp_Success;
            uint capacity = system.GetStringTrackedDeviceProperty((uint)index, ETrackedDeviceProperty.Prop_RenderModelName_String, null, 0, ref error);
            if (capacity > 1)
            {
                steamVRRenderModel.SetDeviceIndex(index);
                steamVRRenderModel.UpdateModel();
            }
        }

        // Update the tracker object
        if (steamVRTrackedObject != null)
        {
            steamVRTrackedObject.SetDeviceIndex(index);
        }
    }



}
