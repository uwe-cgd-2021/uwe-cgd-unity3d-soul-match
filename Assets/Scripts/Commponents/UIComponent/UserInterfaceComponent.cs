using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum LayoutPage
{
    READY,
    GAMEPLAY,
    WIN,
    LOSE
}

public class UserInterfaceComponent : MonoBehaviour
{
    private Dictionary<LayoutPage, List<Canvas>> LayoutCanvas;

    private Text textTime;
    private Text textScore;

    private List<Text> ReadyCountDown = new List<Text>();
    private List<Text> WinCountDown = new List<Text>();
    private List<Text> LoseCountDown = new List<Text>();
    private List<Text> ReadyTextBar = new List<Text>();
    private Dictionary<int, List<Text>> ReadyPlayersText = new Dictionary<int, List<Text>>();

    private List<GameObject> ReadyScreenArrows = new List<GameObject>();

    private CoupleAnimationControl coupleAnimeControl;

    private int currentScore;
    private int TargetScore;

    private LayoutPage currentPage;

    private void Awake()
    {
        string path = gameObject.name + "/";
        string front = "FrontCanvas/";
        string side = "SideCanvas/";
        string ready = "ReadyPage";
        string game = "GameplayPage";
        string win = "WinPage";
        string lose = "LosePage";
        LayoutCanvas = new Dictionary<LayoutPage, List<Canvas>>
        {
           {
               LayoutPage.READY, new List<Canvas>
               {
                     GameObject.Find(path + front + ready).GetComponent<Canvas>(),
                     GameObject.Find(path + side + ready).GetComponent<Canvas>()
               }
           },
           {
               LayoutPage.GAMEPLAY, new List<Canvas>
               {
                     GameObject.Find(path + front + game).GetComponent<Canvas>(),
                     GameObject.Find(path + side + game).GetComponent<Canvas>()
               }
           },
           {
               LayoutPage.WIN, new List<Canvas>
               {
                    GameObject.Find(path + front + win).GetComponent<Canvas>(),
                    GameObject.Find(path + side + win).GetComponent<Canvas>()
               }
           },
           {
               LayoutPage.LOSE, new List<Canvas>
               {
                    GameObject.Find(path + front + lose).GetComponent<Canvas>(),
                    GameObject.Find(path + side + lose).GetComponent<Canvas>()
               }
           }

        };

        textTime = LayoutCanvas[LayoutPage.GAMEPLAY][0].gameObject.transform.Find("Time").GetComponent<Text>();
        textScore = LayoutCanvas[LayoutPage.GAMEPLAY][1].gameObject.transform.Find("Score").GetComponent<Text>();

        foreach (Canvas canvas in LayoutCanvas[LayoutPage.READY])
        {
            ReadyCountDown.Add(canvas.gameObject.transform.Find("Count").GetComponent<Text>());
        }
        foreach (Canvas canvas in LayoutCanvas[LayoutPage.WIN])
        {
            WinCountDown.Add(canvas.gameObject.transform.Find("Count").GetComponent<Text>());
        }
        foreach (Canvas canvas in LayoutCanvas[LayoutPage.LOSE])
        {
            LoseCountDown.Add(canvas.gameObject.transform.Find("Count").GetComponent<Text>());
        }

        foreach (Canvas canvas in LayoutCanvas[LayoutPage.READY])
        {
            ReadyTextBar.Add(canvas.gameObject.transform.Find("StartingBar").GetComponentInChildren<Text>());
        }

        for (int x = 0; x < 4; x++)
        {
            List<Text> playerReadyText = new List<Text>();
            foreach (Canvas canvas in LayoutCanvas[LayoutPage.READY])
            {
                playerReadyText.Add(canvas.gameObject.transform.Find("PlayersReady/Player" + (x + 1)).GetComponentInChildren<Text>());
            }
            ReadyPlayersText.Add(x, playerReadyText);
        }

        foreach (Canvas canvas in LayoutCanvas[LayoutPage.READY])
        {
            ReadyScreenArrows.Add(canvas.gameObject.transform.Find("Arrow").gameObject);
        }

        coupleAnimeControl = gameObject.transform.Find("Renderers").GetComponentInChildren<CoupleAnimationControl>();
    }

    private void Start()
    {
        GoTo(LayoutPage.READY);
    }

    /// <summary>
    /// Switch to other page
    /// </summary>
    /// <param name="page">ready page, game play page, win page, lose page</param>
    public void GoTo(LayoutPage page)
    {
        foreach (LayoutPage thePage in LayoutCanvas.Keys)
        {
            foreach (Canvas canvas in LayoutCanvas[thePage])
            {
                if (canvas != null)
                {
                    canvas.gameObject.SetActive(thePage == page);
                    currentPage = page;
                }
            }
        }
    }

    /// <summary>
    /// Check if the given page is the current page
    /// </summary>
    /// <param name="page">ready page, game play page, win page, lose page</param>
    public bool isInPage(LayoutPage page)
    {
        return page == currentPage;
    }

    /// <summary>
    /// Set game play time UI
    /// </summary>
    /// <param name="second">current progress of the game (second)</param>
    public void setGameplayTime(float second)
    {
        float sec = second % 60.0f;
        int min = (int)(second / 60.0f);
        int hour = (int)(min / 60);
        if (textTime != null)
        {
            textTime.text = string.Format("{0:00}:{1:00}:{2:00}", hour, min, sec);
        }
    }


    private void updateGameplayScoreDisplay()
    {
        if (textScore != null)
        {
            textScore.text = currentScore + "/" + TargetScore;
        }
    }



    /// <summary>
    /// Set game play score UI
    /// </summary>
    /// <param name="score">player current score</param>
    public void setGameplayCurrentScore(int score)
    {
        currentScore = score;
        updateGameplayScoreDisplay();
    }

    /// <summary>
    /// Set game play target or max score UI
    /// </summary>
    /// <param name="score"> player target score</param>
    public void setGameplayCurrentMaximumScore(int score)
    {
        TargetScore = score;
        updateGameplayScoreDisplay();
    }

    /// <summary>
    /// set the canvas screen space camera
    /// </summary>
    /// <param name="frontCanvasCamera"> the front canvas render camera</param>
    /// <param name="sideCanvasCamera"> the side canvas render camera</param>
    public void setCanvasCamera(Camera frontCanvasCamera, Camera sideCanvasCamera)
    {
        Canvas front;
        Canvas side;
        front = gameObject.transform.Find("FrontCanvas").GetComponent<Canvas>();
        front.renderMode = RenderMode.ScreenSpaceCamera;
        front.worldCamera = frontCanvasCamera;
        side = gameObject.transform.Find("SideCanvas").GetComponent<Canvas>();
        side.renderMode = RenderMode.ScreenSpaceCamera;
        side.worldCamera = sideCanvasCamera;
    }

    /// <summary>
    /// Set Count down time  of
    /// </summary>
    /// <param name="second">current progress of the game (second)</param>
    public void setCountDown(LayoutPage layout, float time)
    {
        if (layout == LayoutPage.READY)
        {
            foreach (Text CountDown in ReadyCountDown)
            {
                CountDown.text = ((int)time).ToString();
            }
        }
        else if (layout == LayoutPage.WIN)
        {
            foreach (Text CountDown in WinCountDown)
            {
                CountDown.text = ((int)time).ToString();
            }
        }
        else if (layout == LayoutPage.LOSE)
        {
            foreach (Text CountDown in LoseCountDown)
            {
                CountDown.text = ((int)time).ToString();
            }
        }
    }

    /// <summary>
    /// Display arrow or count down
    /// </summary>
    /// <param name="isCounting">is it counting down</param>
    public void ReadyScreenStartCountDown(bool isCounting)
    {
        if (isCounting)
        {
            foreach (Text text in ReadyCountDown)
            {
                text.gameObject.SetActive(true);
            }
            foreach (GameObject arrow in ReadyScreenArrows)
            {
                arrow.gameObject.SetActive(false);
            }
        }
        else
        {
            foreach (Text text in ReadyCountDown)
            {
                text.gameObject.SetActive(false);
            }
            foreach (GameObject arrow in ReadyScreenArrows)
            {
                arrow.gameObject.SetActive(true);
            }
        }
    }

    /// <summary>
    /// Set text ready or not
    /// </summary>
    /// <param name="playerIndex">Index of the player</param>
    /// <param name="isReady">is the player ready</param>
    public void setPlayerReadyText(int playerIndex, bool isReady)
    {
        foreach (Text text in ReadyPlayersText[playerIndex])
        {
            if (isReady == true)
            {
                text.text = "Ready";
            }
            else
            {
                text.text = "Walk to front screen";
            }
        }
    }

    /// <summary>
    /// Set ready bar text
    /// </summary>
    /// <param name="isReady">Is any player ready</param>
    public void SetReadyBar(bool isReady)
    {
        foreach (Text text in ReadyTextBar)
        {
            if (isReady)
            {
                text.text = "Ready";
            }
            else
            {
                text.text = "Walk to front screen to start game";
            }
        }
    }


    /// <summary>
    /// Set the animation of end game couples
    /// </summary>
    /// <param name="isLose">gameover or not</param>
    public void SetEndGameCoupleAnimation(bool isLose)
    {
        if (isLose)
        {
            coupleAnimeControl.isJump = false;
        }
        else
        {
            coupleAnimeControl.isJump = true;
        }
    }
}
