using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoupleItem : MonoBehaviour
{
    public enum AnimationType
    {
        SPAWN,
        GOAWAY
    }
    
    /// <summary>
    /// Return CoupleItem component of the created couple
    /// </summary>
    /// <param name="coupleType">couple type</param>
    /// <param name="count">The count number on the couple (if it is a counter couple)</param>
    static public CoupleItem createCoupleItem(CoupleType coupleType, int count = 1 )
    {
        GameObject coupleObject = new GameObject("CoupleItem_" + coupleType.ToString());
        CoupleItem coupleItem = coupleObject.AddComponent<CoupleItem>();
        coupleItem.setCoupleType(coupleType);
        coupleItem.playAnimation(AnimationType.SPAWN);
        if (coupleType == CoupleType.COUNTER)
        {
            coupleItem.setCounterNumber(count);
        }
        return coupleItem;
    }
    

    private TextMesh counterTextMesh;
    private GameObject DestroyParticle;
    private bool isRemoved = false;
    private float particleDurationTime = 0.45f;
    private float jumpAnimationTime = 1.0f;
    private GameObject heart;

    public CoupleType typeOfCouple;

    private void Awake()
    {
        counterTextMesh = gameObject.GetComponentInChildren<TextMesh>();
        heart = Resources.Load<GameObject>("Prefabs/Heart/AnimatedHeart");
    }

    /// <summary>
    /// Set the position of the couple
    /// </summary>
    /// <param name="position">The target position of the couple</param>
    /// <param name="isAnimated">will it have a smooth move</param>
    public void setPosition(Vector3 position, bool isAnimated = true)
    {
        if (isAnimated)
        {
            AnimateTransform.animatePosition(this.gameObject, position);
        }
        else
        {
            this.transform.position = position;
        }
    }

    /// <summary>
    /// Set the type of the couple
    /// </summary>
    /// <param name="type">Couple type</param>
    public void setCoupleType(CoupleType type)
    {
        foreach(Transform child in transform)
        {
            Destroy(child.gameObject);
        }
        counterTextMesh = null;

        GameObject couple = null;
        couple = CoupleHelper.getCouplePrefab(type);
        typeOfCouple = type;
        if (couple == null)
        {
            typeOfCouple = CoupleType.NULL;
        }
        couple.transform.parent = this.gameObject.transform;
        StartCoroutine(VisibleCouple(couple));  
    }

    private IEnumerator VisibleCouple(GameObject Couple)
    {
        List<Renderer> meshRenderers = new List<Renderer>();
        foreach (Renderer meshRenderer in Couple.GetComponentsInChildren<Renderer>())
        {
            meshRenderers.Add(meshRenderer);
            meshRenderer.enabled = false;
        }
        yield return new WaitForSeconds(0.15f);
        foreach (Renderer renderer in meshRenderers)
        {
            renderer.enabled = true;
        } 
    }

    /// <summary>
    /// Set the number on the bubble of the counter couple
    /// </summary>
    /// <param name="count">The number on the bubble of the counter couple</param>
    public void setCounterNumber(int count)
    {
        if (typeOfCouple != CoupleType.COUNTER)
        {
            return;
        }
        if (counterTextMesh == null)
        {
            counterTextMesh = gameObject.GetComponentInChildren<TextMesh>();
        }
        counterTextMesh.text = count.ToString();
    }

    /// <summary>
    /// Check is this match with the Couple type
    /// </summary>
    /// <param name="type">Love language that erase</param>
    public bool isMatch(CoupleType type)
    {
        return type == typeOfCouple;
    }
    
    /// <summary>
    /// Play specific animation type
    /// </summary>
    /// <param name="type">The type of the animation spawn, go away</param>
    public void playAnimation(AnimationType type)
    {
        if (type == AnimationType.SPAWN)
        {
            GameObject heartAnimation = Instantiate(heart,gameObject.transform.position + new Vector3(0.0f,4.5f,0.0f), Quaternion.Euler(0.0f, 90.0f, 0.0f), gameObject.transform);
        }
        else
        {
            StartCoroutine(SpawnParticle());
            foreach (Animator animator in gameObject.GetComponentsInChildren(typeof(Animator)))
            {
                animator.SetTrigger("jump");
            }
        }
    }

    private IEnumerator SpawnParticle()
    {
        yield return new WaitForSeconds(jumpAnimationTime);
        GameObject exitParticle = ParticleHelper.playParticle(ParticlesType.COUPLE_LEAVE, particleDurationTime);
        exitParticle.transform.parent = gameObject.transform;
        exitParticle.transform.localPosition = Vector3.zero;
        foreach (Transform child in exitParticle.transform)
        {
            child.transform.localScale = new Vector3(7.0f, 7.0f, 7.0f);
        }
    }
    
    /// <summary>
    /// Destroy the whole Object
    /// </summary>
    public void remove()
    {
        if (isRemoved)
        {
            return;
        }
        isRemoved = true;
        playAnimation(AnimationType.GOAWAY);
        Destroy(gameObject, jumpAnimationTime + particleDurationTime);
    }
}
