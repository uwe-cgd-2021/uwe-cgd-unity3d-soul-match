using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(RawImage))]
public class CameraView : MonoBehaviour
{

    private RawImage imageView = null;

    private UnityAction<Texture2D> onCaptureCallback = null;

    private WebCamTexture webCamTexture = null;
    public int cameraResolutionWidth = 1920;
    public int cameraResolutionHeight = 1080;


    private void Awake()
    {
        this.imageView = this.GetComponent<RawImage>();
        if (this.imageView == null)
        {
            this.imageView = this.gameObject.AddComponent<RawImage>();
        }
    }

    /// <summary>
    /// Start camera
    /// </summary>
    public void startCamera()
    {
        StartCoroutine("startStreamingCamera");
    }

    /// <summary>
    /// Stop camera
    /// </summary>
    public void stopCamera()
    {
        if (webCamTexture != null && webCamTexture.isPlaying)
        {
            webCamTexture.Stop();
        }
    }


    /// <summary>
    /// Capture a picture
    /// </summary>
    public void capture()
    {
        StartCoroutine("captureCameraFrame");
    }

    /// <summary>
    /// Set the callback function which will be fired after captured picture
    /// </summary>
    /// <param name="callback">A callback function with a Texture2D parameter</param>
    public void onCaptured(UnityAction<Texture2D> callback)
    {
        onCaptureCallback = callback;
    }

    private IEnumerator startStreamingCamera()
    {
        //Init camera
        if (webCamTexture == null)
        {

            //Request Authorization
            yield return Application.RequestUserAuthorization(UserAuthorization.WebCam);

            //Find Camera Device and Init WebCam Texture
            if (Application.HasUserAuthorization(UserAuthorization.WebCam))
            {
                WebCamDevice[] devices = WebCamTexture.devices;
                if (devices.Length > 0)
                {
                    imageView.texture = webCamTexture = new WebCamTexture(devices[0].name, cameraResolutionWidth, cameraResolutionHeight, 30);
                }
            }
        }
        yield return null;

        //Start playing
        if (webCamTexture != null && !webCamTexture.isPlaying)
        {
            webCamTexture.Play();
        }
    }

    private IEnumerator captureCameraFrame()
    {
        if (webCamTexture == null || !webCamTexture.isPlaying)
        {
            Debug.LogError("Camera hasn't been started.");
            yield break;
        }

        if (onCaptureCallback == null)
        {
            Debug.LogError("Capture callback hasn't been defined.");
            yield break;
        }

        Texture2D currentFrame = new Texture2D(webCamTexture.width, webCamTexture.height, TextureFormat.ARGB32, false);
        currentFrame.SetPixels(webCamTexture.GetPixels());
        currentFrame.Apply();
        onCaptureCallback(currentFrame);
    }


}