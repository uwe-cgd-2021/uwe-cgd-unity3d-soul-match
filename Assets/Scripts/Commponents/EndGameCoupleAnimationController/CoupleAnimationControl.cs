using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoupleAnimationControl : MonoBehaviour
{
    public bool isJump;
    private List<Animator> animators = new List<Animator>();
    private void Start()
    {
        foreach (Animator anim in gameObject.GetComponentsInChildren<Animator>())
        { 
            animators.Add(anim);
        }
        if (isJump)
        {
            foreach (Animator anim in animators)
            {
                anim.SetTrigger("jump");
            }
        }
    }

    private void Update()
    {
        if (isJump == false)
        {
            return;
        }

        foreach (Animator anim in animators)
        {
            if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.9f)
            {
                anim.SetTrigger("jump");
            }
        }
    }
}
