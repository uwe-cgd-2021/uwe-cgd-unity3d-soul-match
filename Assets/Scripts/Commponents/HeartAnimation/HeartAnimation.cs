using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartAnimation : MonoBehaviour
{
    bool shrink = false;
    float progress = 0f;
    float EnlargeTime = 0.15f;
    float SmallerTime = 0.15f;
    Vector3 NormalSize = new Vector3(1.0f, 1.0f, 1.0f);
    Vector3 MaxSize = new Vector3(11f, 11f, 11f);
    void Update()
    {
        if (shrink == false)
        {
            progress += (Time.unscaledDeltaTime / EnlargeTime);

        }
        else
        {
            progress -= (Time.unscaledDeltaTime / SmallerTime);
        }
        if (progress >= 1.0f)
        {
            shrink = true;
        }

        gameObject.transform.localScale = Vector3.Lerp(NormalSize, MaxSize, progress);

        if (shrink == true && progress <= 0.0f)
        {
            Destroy(gameObject.transform.parent.gameObject);
        }
    }
}
