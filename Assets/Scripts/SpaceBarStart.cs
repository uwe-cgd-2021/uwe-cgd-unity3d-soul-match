using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceBarStart : MonoBehaviour
{
    public ParticleSystem Heart, Star, Light1, Light2;
    public GameObject puzzle;
    private bool spacebarWasPressed = false;
    private Vector3 scaleChange,OriginalScale;


    // Start is called before the first frame update
    void Start()
    {
        OriginalScale = puzzle.transform.localScale;
        scaleChange = new Vector3(-0.08f, -0.08f, -0.08f);
    }

    // Update is called once per frame
    void Update()
    { 
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (spacebarWasPressed == false)
            {
                Heart.Play();
                Star.Play();
                Light1.Play();
                Light2.Play();
                spacebarWasPressed = true;

            }
            else
            {
                puzzle.transform.localScale = OriginalScale;
                spacebarWasPressed = false;
            }
        }
        if (puzzle.transform.localScale.x > 0 && spacebarWasPressed == true)
        {
            puzzle.transform.localScale += scaleChange;
        }
    }
}
