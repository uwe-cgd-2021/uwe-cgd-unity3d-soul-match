using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainController : MonoBehaviour
{
    public Transform CoupleStartingPoint;
    public Transform CoupleEndingPoint;

    public Camera FrontRenderCamera;
    public Camera SideRenderCamera;
    public Camera frontCamera;
    public Camera sideCamera;

    private UserInterfaceComponent UIcomponent = null;
    private GamplayManager gameplayManager = null;
    private CoupleMissionBar missionBar = null;
    private int currentScore;
    private int targetScore = 10;

    private int columnCount;
    private int rowCount;

    private bool startCountDown;
    private float timer;

    private int currentPlayers;

    private void Awake()
    {
        GameObject UI = Instantiate(Resources.Load<GameObject>("Prefabs/UI/UI"));
        UIcomponent = UI.GetComponent<UserInterfaceComponent>();
        if (gameObject.GetComponent<GamplayManager>() == false)
        {
            gameObject.AddComponent<GamplayManager>();
        }
        gameplayManager = gameObject.GetComponent<GamplayManager>();
        if (gameObject.GetComponent<CoupleMissionBar>() == false)
        {
            gameObject.AddComponent<CoupleMissionBar>();
        }
        missionBar = gameObject.GetComponent<CoupleMissionBar>();
        missionBar.SetCoupleMissionBarPosition(CoupleStartingPoint.position, CoupleEndingPoint.position);
    }


    void Start()
    {
        // Get the target score from config
        targetScore = ConfigManager.GetInt(ConfigKey.REQUIRED_COUPLES, 10);

        //Update the UI
        UIcomponent.setGameplayCurrentMaximumScore(targetScore);

        // Register gameplay manager event callback
        gameplayManager.registerOnGameover(onGameover, true);
        gameplayManager.registerOnItemMatched((FallingItemCategory category, FallingItemType type) =>
        {
            if (category == FallingItemCategory.INTEREST)
            {
                missionBar.createMission();
                PlaySound.playAudio(SoundEffectAndMusic.GAMEPLAY_CREATECOUPLE, gameObject);
            }
            else
            {
                missionBar.finishMission(type);
            }
        });

        // Register mission bar event callback
        missionBar.OnMissionFinished.AddListener(() =>
        {
            currentScore++;
            UIcomponent.setGameplayCurrentScore(currentScore);

            if (currentScore >= targetScore)
            {
                onWinGame();
            }
        });

        columnCount = ConfigManager.GetInt(ConfigKey.COLUMNS_COUNT);
        rowCount = ConfigManager.GetInt(ConfigKey.ROWS_COUNT);
        float height = rowCount * 2;
        float width = (columnCount + 1) * 2;
        CameraScaler.SetCamera(FrontRenderCamera, height, width);
        CameraScaler.SetCamera(SideRenderCamera, height, width);

        PlaySound.playAudio(SoundEffectAndMusic.READY_BACKGROUND, gameObject, true);

        ResetTimer();
        startCountDown = true;

        // Enable 3 displays
        DisplayHelper.enableDisplay(3);
    }

    void Update()
    {
        if (startCountDown == true)
        {
            timer -= Time.unscaledDeltaTime;
        }
        else
        {
            ResetTimer();
        }

        if (UIcomponent.isInPage(LayoutPage.READY))
        {
            if (currentPlayers > 0)
            {
                startCountDown = true;
                UIcomponent.SetReadyBar(true);
                for (int x = 0; x < 4; x++)
                {
                    if (gameplayManager.GetReadyPlayersIndex().Contains(x))
                    {
                        UIcomponent.setPlayerReadyText(x, true);
                    }
                    else
                    {
                        UIcomponent.setPlayerReadyText(x, false);
                    }
                }
                UIcomponent.ReadyScreenStartCountDown(true);
            }
            else
            {
                ResetTimer();
                startCountDown = false;
                UIcomponent.SetReadyBar(false);
                UIcomponent.ReadyScreenStartCountDown(false);
            }
            if (currentPlayers != gameplayManager.GetReadyPlayersIndex().Count)
            {
                ResetTimer();
                currentPlayers = gameplayManager.GetReadyPlayersIndex().Count;
            }
            setReadyUICountDown();
            if (timer <= 0.0f)
            {
                startCountDown = false;
                gotoStartGame();
            }
        }
        else if (UIcomponent.isInPage(LayoutPage.GAMEPLAY))
        {

            // Update the timer on UI according to the gampeplay manager
            UIcomponent.setGameplayTime(gameplayManager.getRunningTime());
        }
        else if (UIcomponent.isInPage(LayoutPage.WIN) || UIcomponent.isInPage(LayoutPage.LOSE))
        {
            if (UIcomponent.isInPage(LayoutPage.WIN))
            {
                setWinUICountDown();
            }
            else if (UIcomponent.isInPage(LayoutPage.LOSE))
            {
                setLoseUICountDown();
            }

            if (timer <= 0.0f)
            {
                foreach (AudioSource audioSource in gameObject.GetComponentsInChildren<AudioSource>())
                {
                    Destroy(audioSource);
                }
                ResetTimer();
                gotoReady();
            }
        }

        //Config
        if (Input.GetKeyUp(KeyCode.Alpha0))
        {
            ConfigPanelGUI.show();
        }

        if (Input.GetKeyUp(KeyCode.O))
        {
            PlayAreaDetector.startDefinePlayArea(PlayerTrackerManager.getPlayer(0).gameObject, (Vector3 p1, Vector3 p2) =>
            {
                ConfigManager.saveVector3(ConfigKey.PLAY_AREA_CORNER_LEFT, p1);
                ConfigManager.saveVector3(ConfigKey.PLAY_AREA_CORNER_RIGHT, p2);
            }, null, KeyCode.Return);
        }
    }

    private void gotoReady()
    {
        UIcomponent.GoTo(LayoutPage.READY);
        PlaySound.playAudio(SoundEffectAndMusic.READY_BACKGROUND, gameObject, true);
    }

    private void gotoStartGame()
    {
        if (!gameplayManager.isGamplayRunning())
        {
            PlaySound.playAudio(SoundEffectAndMusic.PRESS_START, gameObject);
            PlaySound.stopAudio(SoundEffectAndMusic.READY_BACKGROUND, gameObject);
            currentScore = 0;
            missionBar.reset();
            UIcomponent.setGameplayCurrentScore(currentScore);
            UIcomponent.GoTo(LayoutPage.GAMEPLAY);
            UIcomponent.setCanvasCamera(frontCamera, sideCamera);

            gameplayManager.startGame();

            PlaySound.playAudio(SoundEffectAndMusic.GAMEPLAY_BACKGROUND, gameObject, true);
        }
    }

    private void gotoEndGame(bool isWin)
    {
        // Stop the game
        gameplayManager.stop();

        PlaySound.stopAudio(SoundEffectAndMusic.GAMEPLAY_BACKGROUND, gameObject);

        //Remove the game objects
        foreach (Transform game in gameObject.transform)
        {
            game.gameObject.SetActive(false);
        }

        UIcomponent.SetEndGameCoupleAnimation(!isWin);

        // Update the UI game
        UIcomponent.GoTo(isWin ? LayoutPage.WIN : LayoutPage.LOSE);

        // start count down after chaning page
        startCountDown = true;
    }

    /// <summary>
    /// A function for handling gameover
    /// </summary>
    private void onGameover()
    {
        gotoEndGame(false);
        PlaySound.playAudio(SoundEffectAndMusic.LOSE_BACKGROUND, gameObject, true);
    }

    /// <summary>
    /// A function for handling winning game
    /// </summary>
    private void onWinGame()
    {
        gotoEndGame(true);
        PlaySound.playAudio(SoundEffectAndMusic.WIN_BACKGROUND, gameObject, true);
    }


    private void ResetTimer()
    {
        timer = 10.0f;
    }

    private void setReadyUICountDown()
    {
        UIcomponent.setCountDown(LayoutPage.READY, timer);
    }

    private void setWinUICountDown()
    {
        UIcomponent.setCountDown(LayoutPage.WIN, timer);
    }

    private void setLoseUICountDown()
    {
        UIcomponent.setCountDown(LayoutPage.LOSE, timer);
    }
}
