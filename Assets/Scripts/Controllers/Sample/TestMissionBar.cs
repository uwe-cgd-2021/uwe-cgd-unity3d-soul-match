using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMissionBar : MonoBehaviour
{

    public CoupleMissionBar misisonBar;
    // Start is called before the first frame update
    void Start()
    {
        misisonBar.OnMissionFinished.AddListener(Write);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            misisonBar.createMission();
        }
        if (Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            misisonBar.finishMission(FallingItemHelper.getRandomType(FallingItemCategory.LOVE_LANGUAGE));
        }
    }

    void Write()
    {
        Debug.Log("matched one thing");
    }
}
