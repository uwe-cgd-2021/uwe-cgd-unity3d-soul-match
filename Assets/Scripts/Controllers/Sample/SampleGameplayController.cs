using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleGameplayController : MonoBehaviour
{
    public GamplayManager gm;

    void Awake()
    {

    }

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < 4; i++)
        {
            PlayerTracker player = PlayerTrackerManager.getPlayer(i);
            if (player != null)
            {
                addDummy(player.transform);
            }
        }
    }

    private void addDummy(Transform target)
    {
        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.transform.SetParent(target, false);
        cube.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);

        GameObject front = GameObject.CreatePrimitive(PrimitiveType.Cube);
        front.transform.SetParent(target, false);
        front.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
        front.transform.localPosition = new Vector3(0, 0, 0.25f);

        GameObject right = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        right.transform.SetParent(target, false);
        right.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
        right.transform.localPosition = new Vector3(0.25f, 0, 0);

        GameObject top = GameObject.CreatePrimitive(PrimitiveType.Cube);
        top.transform.SetParent(target, false);
        top.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        top.transform.localPosition = new Vector3(0, 0.25f, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            gm.startGame();
        }


        if (Input.GetKeyUp(KeyCode.Alpha0))
        {
            ConfigPanelGUI.show();
        }


        if (Input.GetKeyUp(KeyCode.O))
        {
            PlayAreaDetector.startDefinePlayArea(PlayerTrackerManager.getPlayer(0).gameObject, (Vector3 p1, Vector3 p2) =>
            {
                ConfigManager.saveVector3(ConfigKey.PLAY_AREA_CORNER_LEFT, p1);
                ConfigManager.saveVector3(ConfigKey.PLAY_AREA_CORNER_RIGHT, p2);
            }, null, KeyCode.Return);
        }

    }
}
