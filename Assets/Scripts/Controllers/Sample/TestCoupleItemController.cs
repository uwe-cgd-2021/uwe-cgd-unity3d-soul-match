using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCoupleItemController : MonoBehaviour
{
    public CoupleItem couple;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.A))
        {
            //couple.playAnimation(CoupleItem.AnimationType.SPAWN);
            couple = CoupleItem.createCoupleItem(CoupleType.COUNTER, 5);
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            couple.remove();
        }

    }
}
