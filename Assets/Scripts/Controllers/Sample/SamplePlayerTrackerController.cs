using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SamplePlayerTrackerController : MonoBehaviour
{
    public GameObject template;

    private void Awake()
    {

    }

    // Start is called before the first frame update
    void Start()
    {
        //This is sample for understanding how to style the player object. (By adding a model or object)
        PlayerTracker player = PlayerTrackerManager.getPlayer(0);
        if (player != null)
        {
            GameObject cube = GameObject.Instantiate(template);
            cube.transform.SetParent(player.gameObject.transform);
            cube.transform.localPosition = new Vector3();
            cube.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            cube.transform.localRotation = new Quaternion();

            player.debug = true;
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.E))
        {
            ConfigPanelGUI.show();
        }
    }
}