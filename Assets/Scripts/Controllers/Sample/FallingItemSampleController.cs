using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingItemSampleController : MonoBehaviour
{
    FallingItemWall wall;

    private FallingItemWallPlayerProfile.Direction p1Direction = FallingItemWallPlayerProfile.Direction.RIGHT;
    private FallingItemWallPlayerProfile.Direction p2Direction = FallingItemWallPlayerProfile.Direction.RIGHT;
    private int p1ColumnIndex = 1;
    private int p2ColumnIndex = 1;

    private void Awake()
    {
        wall = this.gameObject.GetComponent<FallingItemWall>();
    }

    void Start()
    {
        int[] playerIndexes = { 1, 2 };
        wall.start(FallingItemCategory.INTEREST, 10, 10, playerIndexes);
        wall.registerOnItemMatched((FallingItemCategory category, FallingItemType type) =>
        {
            Debug.Log("Matched : " + type.ToString());
        }, true);

        wall.registerOnPlayerShouldCreateItem((int playerIndex) =>
        {
            wall.createItems(playerIndex);
        }, true);

        wall.registerOnGameover((FallingItemCategory category, FallingItem item) =>
        {
            Debug.Log("End game");
        });

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyUp(KeyCode.Alpha1))
        {
            p1ColumnIndex -= 1;
        }
        if (Input.GetKeyUp(KeyCode.Alpha3))
        {
            p1ColumnIndex += 1;
        }
        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            p1Direction = FallingItemWallPlayerProfile.Direction.UP;
        }
        if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            p1Direction = FallingItemWallPlayerProfile.Direction.DOWN;
        }
        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            p1Direction = FallingItemWallPlayerProfile.Direction.LEFT;
        }
        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            p1Direction = FallingItemWallPlayerProfile.Direction.RIGHT;
        }

        p1ColumnIndex = Mathf.Min(9, Mathf.Max(1, p1ColumnIndex));
        wall.updatePlayerProfile(1, p1ColumnIndex, p1Direction);


        if (Input.GetKeyUp(KeyCode.Alpha9))
        {
            p2ColumnIndex -= 1;
        }
        if (Input.GetKeyUp(KeyCode.Alpha0))
        {
            p2ColumnIndex += 1;
        }
        if (Input.GetKeyUp(KeyCode.W))
        {
            p2Direction = FallingItemWallPlayerProfile.Direction.UP;
        }
        if (Input.GetKeyUp(KeyCode.S))
        {
            p2Direction = FallingItemWallPlayerProfile.Direction.DOWN;
        }
        if (Input.GetKeyUp(KeyCode.A))
        {
            p2Direction = FallingItemWallPlayerProfile.Direction.LEFT;
        }
        if (Input.GetKeyUp(KeyCode.D))
        {
            p2Direction = FallingItemWallPlayerProfile.Direction.RIGHT;
        }


        p2ColumnIndex = Mathf.Min(9, Mathf.Max(1, p2ColumnIndex));
        wall.updatePlayerProfile(2, p2ColumnIndex, p2Direction);





    }
}
