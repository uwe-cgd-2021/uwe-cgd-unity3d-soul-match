using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum Gender
{ 
    MALE,
    FEMALE
}

enum TypeOfCouplePrefab
{
    WHITE_HAT_DUDE,
    BLACK_HAT_DUDE,
    GREEN_HAT_DUDE,
    RED_CAP_DUDE,
    RED_BERET_DUDE,
    BOB_WOMAN,
    PONYTAIL_WOMAN,
    BUN_WOMAN,
    LONG_STRAIGHT_WOMAN,
    SIDE_SWEPT_WOMAN,
    MEDIUM_LENGTH_WOMAN
}

public enum CoupleType
{
    NULL = -1,
    QUALITY_OF_TIME,
    GIFT_GIVING,
    ACTS_OF_SERVICE,
    PHYSICAL_TOUCH,
    WORDS_OF_AFFIRMATION,
    COUNTER
}

public class CoupleHelper : MonoBehaviour
{
    static private Dictionary<Gender, List<TypeOfCouplePrefab>> BoysAndGirls = new Dictionary<Gender, List<TypeOfCouplePrefab>>{
        { 
            Gender.MALE, new List<TypeOfCouplePrefab>
            { 
                TypeOfCouplePrefab.WHITE_HAT_DUDE,
                TypeOfCouplePrefab.BLACK_HAT_DUDE,
                TypeOfCouplePrefab.GREEN_HAT_DUDE,
                TypeOfCouplePrefab.RED_BERET_DUDE,
                TypeOfCouplePrefab.RED_CAP_DUDE
            }
        },
        {
            Gender.FEMALE, new List<TypeOfCouplePrefab>
            {
                TypeOfCouplePrefab.BOB_WOMAN,
                TypeOfCouplePrefab.BUN_WOMAN,
                TypeOfCouplePrefab.LONG_STRAIGHT_WOMAN,
                TypeOfCouplePrefab.MEDIUM_LENGTH_WOMAN,
                TypeOfCouplePrefab.PONYTAIL_WOMAN,
                TypeOfCouplePrefab.SIDE_SWEPT_WOMAN
            }
        }
    };

    static private Dictionary<TypeOfCouplePrefab, string> HumanPrefabName = new Dictionary<TypeOfCouplePrefab, string>{
        {TypeOfCouplePrefab.WHITE_HAT_DUDE, "white-hat"},
        {TypeOfCouplePrefab.BLACK_HAT_DUDE, "black-hat"},
        {TypeOfCouplePrefab.GREEN_HAT_DUDE, "green-hat"},
        {TypeOfCouplePrefab.RED_BERET_DUDE, "red-beret"},
        {TypeOfCouplePrefab.RED_CAP_DUDE, "red-cap"},

        {TypeOfCouplePrefab.BOB_WOMAN, "bob"},
        {TypeOfCouplePrefab.BUN_WOMAN, "bun"},
        {TypeOfCouplePrefab.LONG_STRAIGHT_WOMAN, "long-straight"},
        {TypeOfCouplePrefab.MEDIUM_LENGTH_WOMAN, "medium-length"},
        {TypeOfCouplePrefab.PONYTAIL_WOMAN, "ponytail"},
        {TypeOfCouplePrefab.SIDE_SWEPT_WOMAN, "side-swept"},
    };

    static private Dictionary<CoupleType, string> bubblePrefabName = new Dictionary<CoupleType, string>
    {
        {CoupleType.GIFT_GIVING, "gift-bubble"},
        {CoupleType.ACTS_OF_SERVICE, "service-bubble"},
        {CoupleType.WORDS_OF_AFFIRMATION, "talk-bubble"},
        {CoupleType.QUALITY_OF_TIME, "time-bubble"},
        {CoupleType.PHYSICAL_TOUCH, "touch-bubble"},
        {CoupleType.COUNTER, "num-bubble" }
    };

    static private Dictionary<FallingItemType, CoupleType> MappingFallintItemAndCouple = new Dictionary<FallingItemType, CoupleType>
    {
        {FallingItemType.GIFT_GIVING, CoupleType.GIFT_GIVING },
        {FallingItemType.ACTS_OF_SERVICE, CoupleType.ACTS_OF_SERVICE },
        {FallingItemType.WORDS_OF_AFFIRMATION, CoupleType.WORDS_OF_AFFIRMATION},
        {FallingItemType.QUALITY_OF_TIME, CoupleType.QUALITY_OF_TIME},
        {FallingItemType.PHYSICAL_TOUCH, CoupleType.PHYSICAL_TOUCH}
    };

    static public Dictionary<string, GameObject> CouplePrefabs = new Dictionary<string, GameObject>();

    /// <summary>
    /// Get a random couple with one male and one female
    /// </summary>
    static private List<TypeOfCouplePrefab> RandomCoupleType()
    {
        List<TypeOfCouplePrefab> couple = new List<TypeOfCouplePrefab>();
        couple.Add(BoysAndGirls[Gender.MALE][UnityEngine.Random.Range(0, (BoysAndGirls[Gender.MALE].Count - 1))]);
        couple.Add(BoysAndGirls[Gender.FEMALE][UnityEngine.Random.Range(0, (BoysAndGirls[Gender.FEMALE].Count - 1))]);
        return couple;
    }


    /// <summary>
    /// change a falling item type to CoupleType return CoupleType.NULL (index -1) if not found in CoupleType
    /// </summary>
    /// <param name="itemType">Love Language types or counter</param>
    static public CoupleType changeFallingItemTypeToCoupleType(FallingItemType itemType)
    {
        if (MappingFallintItemAndCouple.ContainsKey(itemType))
        {
            return MappingFallintItemAndCouple[itemType];
        }
        else
        {
            return CoupleType.NULL;
        }
    }

    /// <summary>
    /// Get a random couple gameobject with bubble of love language type
    /// </summary>
    /// <param name="itemType">Love Language types  Please Only Enter Love Language TYPE
    /// (QUALITY_OF_TIME,GIFT_GIVING,ACTS_OF_SERVICE,PHYSICAL_TOUCH,WORDS_OF_AFFIRMATION) </param>
    static public GameObject getCouplePrefab(CoupleType itemType)
    {
        GameObject malePrefab = null;
        GameObject femalePrefab = null;
        GameObject bubblePrefab = null;

        GameObject couple = null;
        
        List<TypeOfCouplePrefab> randomCoupleType = RandomCoupleType();

        if (itemType == CoupleType.NULL)
        {
            return null;
        }


        if (CouplePrefabs.ContainsKey(HumanPrefabName[randomCoupleType[0]]))
        {
            malePrefab = CouplePrefabs[HumanPrefabName[randomCoupleType[0]]];
        }
        else
        {
            CouplePrefabs.Add(HumanPrefabName[randomCoupleType[0]], Resources.Load<GameObject>("Prefabs/Couple/" + HumanPrefabName[randomCoupleType[0]]));
            malePrefab = CouplePrefabs[HumanPrefabName[randomCoupleType[0]]];
        }

        if (CouplePrefabs.ContainsKey(HumanPrefabName[randomCoupleType[1]]))
        {
            femalePrefab = CouplePrefabs[HumanPrefabName[randomCoupleType[1]]];
        }
        else
        {
            CouplePrefabs.Add(HumanPrefabName[randomCoupleType[1]], Resources.Load<GameObject>("Prefabs/Couple/" + HumanPrefabName[randomCoupleType[1]]));
            femalePrefab = CouplePrefabs[HumanPrefabName[randomCoupleType[1]]];
        }        

        if (CouplePrefabs.ContainsKey(bubblePrefabName[itemType]))
        {
            bubblePrefab = CouplePrefabs[bubblePrefabName[itemType]];
        }
        else
        {
            CouplePrefabs.Add(bubblePrefabName[itemType], Resources.Load<GameObject>("Prefabs/bubbles/" + bubblePrefabName[itemType]));
            bubblePrefab = CouplePrefabs[bubblePrefabName[itemType]];
        }

        if (malePrefab != null && femalePrefab != null && bubblePrefab != null)
        {
            couple = new GameObject("Couple_" + itemType.ToString());
            Instantiate(malePrefab, new Vector3(2.6f, 0, 0), Quaternion.Euler(0.0f, 45.0f, 0.0f), couple.transform);
            Instantiate(femalePrefab, new Vector3(-1.4f, 0, 3.54f), Quaternion.Euler(0.0f, 45.0f, 0.0f), couple.transform);
            Instantiate(bubblePrefab, new Vector3(5.2f, 1.5f, 2.5f), Quaternion.Euler(0.0f, 90.0f, 0.0f), couple.transform);
        }

        return couple;
    }
}
