using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TransformHelper
{
    public static void animatePosition(GameObject target, Vector3 position, float speed = 1.0f)
    {
        if (target != null && target.transform != null)
        {
            if (Vector3.Distance(target.transform.position, position) > 0.1f)
            {
                target.transform.position = Vector3.Lerp(target.transform.position, position, Time.unscaledDeltaTime * speed);
            }
            else
            {
                target.transform.position = position;
            }
        }
    }
}
