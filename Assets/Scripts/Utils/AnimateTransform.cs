using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimateTransform : MonoBehaviour
{

    private static AnimateTransform getAnimateTransform(GameObject target)
    {
        AnimateTransform animateTransform = null;
        if (target != null)
        {
            animateTransform = target.GetComponent<AnimateTransform>();
            if (animateTransform == null)
            {
                animateTransform = target.AddComponent<AnimateTransform>();
                animateTransform.initEulerAngles();
            }
        }
        return animateTransform;
    }


    /// <summary>
    /// A IEnumerator for fire action after delay
    /// Sample usage: StartCoroutine(AnimateTransform.doActionAfterDelay(() =>{Debug.Log("Sample");}, 0.25f));
    /// <param name="action">The function to do after delay</param>
    /// <param name="duration">The delay in second</param>
    /// </summary>
    public static IEnumerator doActionAfterDelay(UnityAction action, float delay = 0.2f)
    {
        yield return new WaitForSeconds(delay);
        action();
    }

    /// <summary>
    /// Animate the position of the target to the given position in a specific duration in second
    /// <param name="target">The target game object to be updated</param>
    /// <param name="position">The position of the destination</param>
    /// <param name="duration">The duration in second</param>
    /// </summary>
    public static void animatePosition(GameObject target, Vector3 position, float duration = 0.2f)
    {
        AnimateTransform animateTransform = getAnimateTransform(target);
        if (animateTransform != null)
        {
            animateTransform.animatePosition(position, duration);
        }
    }

    /// <summary>
    /// Animate the localPosition of the target to the given locallocalPosition in a specific duration in second
    /// <param name="target">The target game object to be updated</param>
    /// <param name="localPosition">The local position of the destination</param>
    /// <param name="duration">The duration in second</param>
    /// </summary>
    public static void animateLocalPosition(GameObject target, Vector3 localPosition, float duration = 0.2f)
    {
        AnimateTransform animateTransform = getAnimateTransform(target);
        if (animateTransform != null)
        {
            animateTransform.animateLocalPosition(localPosition, duration);
        }
    }

    /// <summary>
    /// Animate the localScale of the target to the given locallocalScale in a specific duration in second
    /// <param name="target">The target game object to be updated</param>
    /// <param name="localScale">The local position of the destination</param>
    /// <param name="duration">The duration in second</param>
    /// </summary>
    public static void animateLocalScale(GameObject target, Vector3 localScale, float duration = 0.2f)
    {
        AnimateTransform animateTransform = getAnimateTransform(target);
        if (animateTransform != null)
        {
            animateTransform.animatelocalScale(localScale, duration);
        }
    }

    /// <summary>
    /// Animate the rotation of the target to the given Euler Angles in a specific duration in second
    /// <param name="target">The target game object to be updated</param>
    /// <param name="eulerAngles">The euler angles</param>
    /// <param name="duration">The duration in second</param>
    /// </summary>
    public static void animateEulerAngles(GameObject target, Vector3 eulerAngles, float duration = 0.2f)
    {
        AnimateTransform animateTransform = getAnimateTransform(target);
        if (animateTransform != null)
        {
            animateTransform.animateEulerAngles(eulerAngles, duration);
        }
    }

    private Vector3 positionTarget, positionStart;
    float positionTotalDuration, positionCurrentDuration = 0.0f;

    private Vector3 localPositionTarget, localPositionStart;
    float localPositionTotalDuration, localPositionCurrentDuration = 0.0f;

    private Vector3 localScaleTarget, localScaleStart;
    float localScaleTotalDuration, localScaleCurrentDuration = 0.0f;

    private Vector3 eulerAnglesTarget, eulerAnglesCurrent, eulerAnglesStart;
    float eulerAnglesTotalDuration, eulerAnglesCurrentDuration = 0.0f;

    void Update()
    {
        // Animate Position if need
        if (positionCurrentDuration < 1 && positionTotalDuration > 0.0f)
        {
            positionCurrentDuration += Time.unscaledDeltaTime / positionTotalDuration;
            if (positionCurrentDuration >= 1.0f)
            {
                positionCurrentDuration = 1.0f;
            }
            this.gameObject.transform.position = Vector3.Lerp(positionStart, positionTarget, positionCurrentDuration);
        }

        // Animate localScale if need
        if (localScaleCurrentDuration < 1 && localScaleTotalDuration > 0.0f)
        {
            localScaleCurrentDuration += Time.unscaledDeltaTime / localScaleTotalDuration;
            if (localScaleCurrentDuration >= 1.0f)
            {
                localScaleCurrentDuration = 1.0f;
            }
            this.gameObject.transform.localScale = Vector3.Lerp(localScaleStart, localScaleTarget, localScaleCurrentDuration);
        }

        // Animate localPosition if need
        if (localPositionCurrentDuration < 1 && localPositionTotalDuration > 0.0f)
        {
            localPositionCurrentDuration += Time.unscaledDeltaTime / localPositionTotalDuration;
            if (localPositionCurrentDuration >= 1.0f)
            {
                localPositionCurrentDuration = 1.0f;
            }
            this.gameObject.transform.localPosition = Vector3.Lerp(localPositionStart, localPositionTarget, localPositionCurrentDuration);
        }


        // Animate Euler Angles if need
        if (eulerAnglesCurrentDuration < 1 && eulerAnglesTotalDuration > 0.0f)
        {
            eulerAnglesCurrentDuration += Time.unscaledDeltaTime / eulerAnglesTotalDuration;
            if (eulerAnglesCurrentDuration >= 1.0f)
            {
                eulerAnglesCurrentDuration = 1.0f;
            }
            eulerAnglesCurrent = Vector3.Lerp(eulerAnglesStart, eulerAnglesTarget, eulerAnglesCurrentDuration);
            this.gameObject.transform.eulerAngles = eulerAnglesCurrent;
        }
    }

    private void animatePosition(Vector3 position, float duration)
    {
        positionCurrentDuration = 0.0f;
        positionTotalDuration = duration;
        positionStart = this.transform.position;
        positionTarget = position;
    }

    private void animateLocalPosition(Vector3 localPosition, float duration)
    {
        localPositionCurrentDuration = 0.0f;
        localPositionTotalDuration = duration;
        localPositionStart = this.transform.localPosition;
        localPositionTarget = localPosition;
    }

    private void animatelocalScale(Vector3 localScale, float duration)
    {
        localScaleCurrentDuration = 0.0f;
        localScaleTotalDuration = duration;
        localScaleStart = this.transform.localScale;
        localScaleTarget = localScale;
    }

    private void initEulerAngles()
    {
        eulerAnglesCurrent = this.gameObject.transform.eulerAngles;
    }
    private void animateEulerAngles(Vector3 eulerAngles, float duration)
    {
        eulerAnglesCurrentDuration = 0.0f;
        eulerAnglesTotalDuration = duration;
        eulerAnglesStart = eulerAnglesCurrent;
        eulerAnglesTarget = eulerAngles;
    }

}
