using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RuntimeStorage : MonoBehaviour
{
    static private GameObject rtGameObject = null;
    static private RuntimeStorage rt = null;

    private UnityEvent<string, string, string> eventStringUpdated = new UnityEvent<string, string, string>();
    private UnityEvent<string, int, int> eventIntUpdated = new UnityEvent<string, int, int>();
    private UnityEvent<string, float, float> eventFloatUpdated = new UnityEvent<string, float, float>();
    private UnityEvent<string, GameObject, GameObject> eventGameObjectUpdated = new UnityEvent<string, GameObject, GameObject>();

    private Dictionary<string, string> dicForString = new Dictionary<string, string>();
    private Dictionary<string, int> dicForInt = new Dictionary<string, int>();
    private Dictionary<string, float> dicForFloat = new Dictionary<string, float>();
    private Dictionary<string, GameObject> dicForGameObject = new Dictionary<string, GameObject>();

    static private RuntimeStorage shared()
    {
        if (rtGameObject == null)
        {
            rtGameObject = new GameObject("RuntimeStorageGameObject-DontDestroyOnLoad");
            DontDestroyOnLoad(rtGameObject);
            rt = rtGameObject.AddComponent<RuntimeStorage>();
        }
        return rt;
    }

    /// <summary>
    /// Returns the value corresponding to key if it exists, otherwise return the given default value if it's given.
    /// </summary>
    static public string getString(string key, string defaultValue = null)
    {
        string value;
        if (!RuntimeStorage.shared().dicForString.TryGetValue(key, out value))
        {
            return defaultValue;
        }
        return value;
    }

    /// <summary>
    /// Sets a string value by the given key.
    /// </summary>
    /// <param name="key">The given key</param>
    /// <param name="value">The given value</param>
    static public void setString(string key, string value)
    {
        string oldValue = getString(key);
        RuntimeStorage.shared().dicForString[key] = value;
        RuntimeStorage.shared().eventStringUpdated.Invoke(key, oldValue, value);
    }

    /// <summary>
    /// Add callback while will be fired when any string update.
    /// </summary>
    /// <param name="UnityAction">A callback function with parameters - callback(string key, string oldValue, string newValue)</param>
    static public void onStringUpdate(UnityAction<string, string, string> callback)
    {
        RuntimeStorage.shared().eventStringUpdated.AddListener(callback);
    }

    /// <summary>
    /// Returns the value corresponding to key if it exists, otherwise return the given default value if it's given.
    /// </summary>
    static public int getInt(string key, int defaultValue = 0)
    {
        int value;
        if (!RuntimeStorage.shared().dicForInt.TryGetValue(key, out value))
        {
            return defaultValue;
        }
        return value;
    }
    /// <summary>
    /// Sets a int value by the given key.
    /// </summary>
    /// <param name="key">The given key</param>
    /// <param name="value">The given value</param>
    static public void setInt(string key, int value)
    {
        int oldValue = getInt(key);
        RuntimeStorage.shared().dicForInt[key] = value;
        RuntimeStorage.shared().eventIntUpdated.Invoke(key, oldValue, value);
    }

    /// <summary>
    /// Add callback while will be fired when any int update.
    /// </summary>
    /// <param name="UnityAction">A callback function with parameters - callback(string key, int oldValue, int newValue)</param>
    static public void onIntUpdate(UnityAction<string, int, int> callback)
    {
        RuntimeStorage.shared().eventIntUpdated.AddListener(callback);
    }

    /// <summary>
    /// Returns the value corresponding to key if it exists, otherwise return the given default value if it's given.
    /// </summary>
    static public float getFloat(string key, float defaultValue = 0.0f)
    {
        float value;
        if (!RuntimeStorage.shared().dicForFloat.TryGetValue(key, out value))
        {
            return defaultValue;
        }
        return value;
    }

    /// <summary>
    /// Sets a float value by the given key.
    /// </summary>
    /// <param name="key">The given key</param>
    /// <param name="value">The given value</param>
    static public void setFloat(string key, float value)
    {
        float oldValue = getFloat(key);
        RuntimeStorage.shared().dicForFloat[key] = value;
        RuntimeStorage.shared().eventFloatUpdated.Invoke(key, oldValue, value);

    }

    /// <summary>
    /// Add callback while will be fired when any float update.
    /// </summary>
    /// <param name="UnityAction">A callback function with parameters - callback(string key, float oldValue, float newValue)</param>
    static public void onFloatUpdate(UnityAction<string, float, float> callback)
    {
        RuntimeStorage.shared().eventFloatUpdated.AddListener(callback);
    }

    /// <summary>
    /// Returns the value corresponding to key if it exists, otherwise return the given default value if it's given.
    /// </summary>
    static public GameObject getGameObject(string key, GameObject defaultValue = null)
    {
        GameObject value;
        if (!RuntimeStorage.shared().dicForGameObject.TryGetValue(key, out value))
        {
            return defaultValue;
        }
        return value;
    }

    /// <summary>
    /// Sets a GameObject value by the given key.
    /// </summary>
    /// <param name="key">The given key</param>
    /// <param name="value">The given value</param>
    static public void setGameObject(string key, GameObject value)
    {
        GameObject oldValue = getGameObject(key);
        RuntimeStorage.shared().dicForGameObject[key] = value;
        RuntimeStorage.shared().eventGameObjectUpdated.Invoke(key, oldValue, value);
    }


    /// <summary>
    /// Add callback while will be fired when any GameObject update.
    /// </summary>
    /// <param name="UnityAction">A callback function with parameters - callback(string key, GameObject oldValue, GameObject newValue)</param>
    static public void onGameObjectUpdate(UnityAction<string, GameObject, GameObject> callback)
    {
        RuntimeStorage.shared().eventGameObjectUpdated.AddListener(callback);
    }


}