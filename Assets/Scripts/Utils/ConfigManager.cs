using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


[System.Serializable]
public class ConfigItem
{
    public string key;
    public string value;
}

[System.Serializable]
public class Config
{
    public int version;

    public ConfigItem[] configItems;
}

public enum ConfigKey
{
    PLAYER_1_TRACKER_INDEX,
    PLAYER_2_TRACKER_INDEX,
    PLAYER_3_TRACKER_INDEX,
    PLAYER_4_TRACKER_INDEX,
    COLUMNS_COUNT,
    ROWS_COUNT,
    PLAY_AREA_CORNER_LEFT,
    PLAY_AREA_CORNER_RIGHT,
    PLAY_AREA_FLIP_X,
    PLAY_AREA_FLIP_Z,
    REQUIRED_COUPLES,
};



static public class ConfigManager
{
    private static Config config = null;

    private static UnityEvent eventOnSaved = new UnityEvent();


    private static Dictionary<ConfigKey, string> keyMapping = new Dictionary<ConfigKey, string>(){
        { ConfigKey.PLAYER_1_TRACKER_INDEX, "player1TrackerIndex" },
        { ConfigKey.PLAYER_2_TRACKER_INDEX, "player2TrackerIndex" },
        { ConfigKey.PLAYER_3_TRACKER_INDEX, "player3TrackerIndex" },
        { ConfigKey.PLAYER_4_TRACKER_INDEX, "player4TrackerIndex" },
        { ConfigKey.COLUMNS_COUNT, "columnsCount" },
        { ConfigKey.ROWS_COUNT, "rowsCount" },
        { ConfigKey.PLAY_AREA_CORNER_LEFT, "playAreaCornerLeft" },
        { ConfigKey.PLAY_AREA_CORNER_RIGHT, "playAreaCornerRight" },
        { ConfigKey.PLAY_AREA_FLIP_X, "playAreaFlipX" },
        { ConfigKey.PLAY_AREA_FLIP_Z, "playAreaFlipZ" },
        { ConfigKey.REQUIRED_COUPLES, "requiredCouples" },
    };

    //Load saved config
    private static void LoadConfig(bool isForceLoadFromResources = false)
    {

        //Return if config is already loaded
        if (config != null && !isForceLoadFromResources)
        {
            return;
        }

        //Get the saved config JSON
        string strConfigJSON = GetConfigJSONFromPath(GetConfigSavingFilePath());
        Config savedConfig = GetConfigByJSON(strConfigJSON);

        //Get the default config JSON and Define Config Item
        string strDefaultConfigJSON = GetConfigJSONFromResources();
        Config defaultConfig = GetConfigByJSON(strDefaultConfigJSON);

        //Check version if strConfigJSON not null
        if (savedConfig != null)
        {
            if (savedConfig.version != defaultConfig.version)
            {
                savedConfig = null;
            }
        }

        //Set which config item will use
        if (savedConfig != null && !isForceLoadFromResources)
        {
            config = savedConfig;
        }
        else
        {
            //Using default config and save the config JSON
            config = defaultConfig;
            SaveConfigJSON(strDefaultConfigJSON);
        }
    }


    //Set current config by json string
    private static void SaveConfigJSON(string strJSON)
    {
        //Save JSON string
        System.IO.File.WriteAllText(GetConfigSavingFilePath(), strJSON);
    }

    //Get config item by JSON String
    private static Config GetConfigByJSON(string strJSON)
    {
        if (strJSON != null)
        {
            if (strJSON.Length > 0)
            {
                return JsonUtility.FromJson<Config>(strJSON);
            }
        }
        return null;
    }

    private static string ConvertConfigToJson(Config Config)
    {
        return JsonUtility.ToJson(Config);
    }

    //Load config JSON from path
    private static string GetConfigJSONFromPath(string path)
    {
        if (System.IO.File.Exists(path))
        {
            return System.IO.File.ReadAllText(path);
        }
        return null;
    }

    //Load config JSON from Resources
    private static string GetConfigJSONFromResources()
    {
        TextAsset textAssetConfig = Resources.Load<TextAsset>("config");
        return textAssetConfig.text;
    }

    private static ConfigItem GetConfigItem(string key)
    {
        LoadConfig();
        foreach (ConfigItem item in config.configItems)
        {
            if (item.key == key)
            {
                return item;
            }
        }
        return null;
    }


    /// <summary>
    /// Get config saving file path
    /// </summary>
    /// <returns>Returns {string} config file saving path</returns>
    public static string GetConfigSavingFilePath()
    {
        return Application.persistentDataPath + "/config.json";
    }

    /// <summary>
    /// Get current config.
    /// Auto load saved config if exist, otherwise a new config item will be created.
    /// Never store the config value into your own component, because it will be changed by others.
    /// Instead, please use other functions to get the values, such as `GetInt`, `GetFloat`, `GetString` and `GetVector3`.
    /// </summary>
    /// <returns>Returns {Config} current config</returns>
    public static Config GetConfig()
    {
        LoadConfig();
        return config;
    }

    /// <summary>
    /// Reloads the config from resource
    /// </summary>
    public static void ReloadConfig()
    {
        LoadConfig(true);
    }

    /// <summary>
    /// Reloads the config.
    /// </summary>
    /// <param name="config">The {Config} config to be saved</param>
    public static void SaveConfig(Config config)
    {
        ConfigManager.config = config;
        SaveConfigJSON(ConvertConfigToJson(config));
        eventOnSaved.Invoke();
    }

    /// <summary>
    /// Get int value by key
    /// </summary>
    /// <param name="key">The {string} key of the config</param>
    /// <param name="defaultValue">The {int} default value</param>
    public static int GetInt(string key, int defaultValue = 0)
    {
        ConfigItem item = GetConfigItem(key);
        if (item != null && item.value != null && item.value.Length > 0)
        {
            return int.Parse(item.value);
        }
        return defaultValue;
    }

    /// <summary>
    /// Get int value by key
    /// </summary>
    /// <param name="key">The {ConfigKey} key of the config</param>
    /// <param name="defaultValue">The {int} default value</param>
    public static int GetInt(ConfigKey key, int defaultValue = 0)
    {
        return GetInt(getConfigKey(key), defaultValue);
    }

    /// <summary>
    /// Get float value by key
    /// </summary>
    /// <param name="key">The {string} key of the config</param>
    /// <param name="defaultValue">The {float} default value</param>
    public static float GetFloat(string key, float defaultValue = 0.0f)
    {
        ConfigItem item = GetConfigItem(key);
        if (item != null && item.value != null && item.value.Length > 0)
        {
            return float.Parse(item.value);
        }
        return defaultValue;
    }

    /// <summary>
    /// Get float value by key
    /// </summary>
    /// <param name="key">The {ConfigKey} key of the config</param>
    /// <param name="defaultValue">The {float} default value</param>
    public static float GetFloat(ConfigKey key, float defaultValue = 0.0f)
    {
        return GetFloat(getConfigKey(key), defaultValue);
    }

    /// <summary>
    /// Get string value by key
    /// </summary>
    /// <param name="key">The {string} key of the config</param>
    /// <param name="defaultValue">The {string} default value</param>
    public static string GetString(string key, string defaultValue = "")
    {
        ConfigItem item = GetConfigItem(key);
        if (item != null && item.value != null && item.value.Length > 0)
        {
            return item.value;
        }
        return defaultValue;
    }

    /// <summary>
    /// Get string value by key
    /// </summary>
    /// <param name="key">The {ConfigKey} key of the config</param>
    /// <param name="defaultValue">The {string} default value</param>
    public static string GetString(ConfigKey key, string defaultValue = "")
    {
        return GetString(getConfigKey(key), defaultValue);
    }

    /// <summary>
    /// Get Vector3 value by key
    /// </summary>
    /// <param name="key">The {string} key of the config</param>
    /// <param name="defaultValue">The {Vector3} default value</param>
    public static Vector3 GetVector3(string key, Vector3 defaultValue = new Vector3())
    {
        ConfigItem item = GetConfigItem(key);
        if (item != null && item.value != null && item.value.Length > 0)
        {
            string[] values = item.value.Split(',');
            if (values.Length == 3)
            {
                return new Vector3(float.Parse(values[0]), float.Parse(values[1]), float.Parse(values[2]));
            }
        }
        return defaultValue;
    }

    /// <summary>
    /// Get Vector3 value by key
    /// </summary>
    /// <param name="key">The {ConfigKey} key of the config</param>
    /// <param name="defaultValue">The {Vector3} default value</param>
    public static Vector3 GetVector3(ConfigKey key, Vector3 defaultValue = new Vector3())
    {
        return GetVector3(getConfigKey(key), defaultValue);
    }

    /// <summary>
    /// Set string value by key
    /// </summary>
    /// <param name="key">The {string} key of the config</param>
    /// <param name="value">The {string} value</param>
    public static void saveString(string key, string value)
    {
        LoadConfig();
        for (int i = 0; i < config.configItems.Length; i++)
        {
            if (config.configItems[i].key == key)
            {
                config.configItems[i].value = value;
            }
        }
        SaveConfig(config);
    }

    /// <summary>
    /// Set string value by key
    /// </summary>
    /// <param name="key">The {ConfigKey} key of the config</param>
    /// <param name="value">The {string} value</param>
    public static void saveString(ConfigKey key, string value)
    {
        saveString(getConfigKey(key), value);
    }

    /// <summary>
    /// Set int value by key
    /// </summary>
    /// <param name="key">The {string} key of the config</param>
    /// <param name="value">The {int} value</param>
    public static void saveInt(string key, int value)
    {
        saveString(key, value.ToString());
    }

    /// <summary>
    /// Set int value by key
    /// </summary>
    /// <param name="key">The {ConfigKey} key of the config</param>
    /// <param name="value">The {int} value</param>
    public static void saveInt(ConfigKey key, int value)
    {
        saveInt(getConfigKey(key), value);
    }

    /// <summary>
    /// Set float value by key
    /// </summary>
    /// <param name="key">The {string} key of the config</param>
    /// <param name="value">The {float} value</param>
    public static void saveFloat(string key, float value)
    {
        saveString(key, value.ToString());
    }

    /// <summary>
    /// Set float value by key
    /// </summary>
    /// <param name="key">The {ConfigKey} key of the config</param>
    /// <param name="value">The {float} value</param>
    public static void saveFloat(ConfigKey key, float value)
    {
        saveFloat(getConfigKey(key), value);
    }


    /// <summary>
    /// Set Vector3 value by key
    /// </summary>
    /// <param name="key">The {string} key of the config</param>
    /// <param name="value">The {Vector3} value</param>
    public static void saveVector3(string key, Vector3 value)
    {
        saveString(key, value.x + "," + value.y + "," + value.z);
    }

    /// <summary>
    /// Set Vector3 value by key
    /// </summary>
    /// <param name="key">The {ConfigKey} key of the config</param>
    /// <param name="value">The {Vector3} value</param>
    public static void saveVector3(ConfigKey key, Vector3 value)
    {
        saveVector3(getConfigKey(key), value);
    }

    /// <summary>
    /// Get config key by enum
    /// </summary>
    /// <param name="key">The {ConfigKey} key of the config</param>
    public static string getConfigKey(ConfigKey key)
    {
        return keyMapping.ContainsKey(key) ? keyMapping[key] : null;
    }


    public static void registerConfigSaved(UnityAction onConfigSavedCallback)
    {
        eventOnSaved.AddListener(onConfigSavedCallback);
    }

    public static void UnregisteredConfigSaved(UnityAction onConfigSavedCallback)
    {
        eventOnSaved.RemoveListener(onConfigSavedCallback);
    }


}


