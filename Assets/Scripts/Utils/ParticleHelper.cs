using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ParticlesType
{ 
    FALLINGITEM_HEART,
    FALLINGITEM_STAR,
    COUPLE_LEAVE
}

public class ParticleHelper : MonoBehaviour
{
    private static Dictionary<ParticlesType, string> particlePath = new Dictionary<ParticlesType, string>
    {
        {ParticlesType.FALLINGITEM_HEART, "Prefabs/Particle_Heart"},
        {ParticlesType.FALLINGITEM_STAR, "Prefabs/Particle_Star_B"},
        {ParticlesType.COUPLE_LEAVE, "Prefabs/ParticleForCoupleExit"}
    };

    private static Dictionary<ParticlesType, GameObject> LoadedParticles = new Dictionary<ParticlesType, GameObject>();

    /// <summary>
    /// Play a particle
    /// </summary>
    /// <param name="particle">the particle type</param>
    /// <param name="position">the position of creating the particle</param>
    /// <param name="destroyTime">after this time the particle will self destroy</param>
    /// <param name="parent">the parent of the particle</param>
    public static GameObject playParticle(ParticlesType particle,float destroyTime)
    {
        GameObject parti = null;
        if (LoadedParticles.ContainsKey(particle) == false)
        {
            LoadedParticles.Add(particle, Resources.Load<GameObject>(particlePath[particle])) ;
        }
        parti = Instantiate(LoadedParticles[particle]);
        Destroy(parti, destroyTime);
        return parti;
    }
}
