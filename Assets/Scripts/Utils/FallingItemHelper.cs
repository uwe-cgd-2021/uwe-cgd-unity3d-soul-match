using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FallingItemCategory
{
    INTEREST,
    LOVE_LANGUAGE
}

public enum FallingItemType
{
    VIDEO_GAME,
    TRAVEL,
    READING,
    FOODIE,
    MOVIE,
    SPORT,
    MUSIC,
    QUALITY_OF_TIME,
    GIFT_GIVING,
    ACTS_OF_SERVICE,
    PHYSICAL_TOUCH,
    WORDS_OF_AFFIRMATION
}




static public class FallingItemHelper
{
    static private Dictionary<FallingItemCategory, List<FallingItemType>> types = new Dictionary<FallingItemCategory, List<FallingItemType>>{
        {
            FallingItemCategory.INTEREST, new List<FallingItemType>{
                FallingItemType.VIDEO_GAME,
                FallingItemType.TRAVEL,
                FallingItemType.READING,
                FallingItemType.FOODIE,
                FallingItemType.MOVIE,
                FallingItemType.SPORT,
                FallingItemType.MUSIC
            }
        },
        {
            FallingItemCategory.LOVE_LANGUAGE, new List<FallingItemType>{
                FallingItemType.QUALITY_OF_TIME,
                FallingItemType.GIFT_GIVING,
                FallingItemType.ACTS_OF_SERVICE,
                FallingItemType.PHYSICAL_TOUCH,
                FallingItemType.WORDS_OF_AFFIRMATION
            }
        }
    };


    static private Dictionary<FallingItemType, string> typeNames = new Dictionary<FallingItemType, string>{
        {FallingItemType.VIDEO_GAME, "video-game"},
        {FallingItemType.TRAVEL, "travel"},
        {FallingItemType.READING, "reading" },
        {FallingItemType.FOODIE, "foodie" },
        {FallingItemType.MOVIE, "movie" },
        {FallingItemType.SPORT, "sport" },
        {FallingItemType.MUSIC, "music" },
        {FallingItemType.QUALITY_OF_TIME, "quality-of-time" },
        {FallingItemType.GIFT_GIVING, "gift-giving" },
        {FallingItemType.ACTS_OF_SERVICE, "acts-of-service" },
        {FallingItemType.PHYSICAL_TOUCH, "physical-touch" },
        {FallingItemType.WORDS_OF_AFFIRMATION, "words-of-affirmation" }
    };

    static public Dictionary<string, GameObject> itemPrefabs = new Dictionary<string, GameObject>();
    /// <summary>
    /// Get a random type from the category
    /// </summary>
    /// <param name="category">Interest or Love Language</param>
    static public FallingItemType getRandomType(FallingItemCategory category)
    {
        return types[category][Random.Range(0, (types[category].Count - 1))];
    }

    /// <summary>
    /// Get two random type from the category, return a List contain two different FallingItemType
    /// </summary>
    /// <param name="category">Interest or Love Language</param>
    static public List<FallingItemType> getRandomTypes(FallingItemCategory category)
    {
        List<FallingItemType> twoDifferentType = new List<FallingItemType>();
        do
        {
            FallingItemType itemType = getRandomType(category);
            if (twoDifferentType.Contains(itemType) == false)
            {
                twoDifferentType.Add(itemType);
            }
        } while (twoDifferentType.Count < 2);
        return twoDifferentType;
    }

    /// <summary>
    /// Return a GameObjet of a falling item prefab
    /// </summary>
    /// <param name="playerIndex">Index of player (0, 1, 2, 3)</param>
    /// <param name="type">FallingItemType (video game, sport etc.)</param>
    static public GameObject getFallingItemPrefab(int playerIndex, FallingItemType type)
    {
        GameObject prefab = null;

        string typeName = typeNames[type];

        string prefabName = typeName + "-player-" + playerIndex;

        if (itemPrefabs.ContainsKey(prefabName))
        {
            prefab = itemPrefabs[prefabName];
        }
        else
        { 
            prefab = Resources.Load<GameObject>("Prefabs/FallingItems/" + prefabName);
            itemPrefabs.Add(prefabName, prefab);
        }

        return prefab != null ? Object.Instantiate(prefab) : null;
    }


}
