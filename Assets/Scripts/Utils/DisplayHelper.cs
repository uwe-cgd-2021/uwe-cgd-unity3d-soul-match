using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayHelper
{
    /// <summary>
    /// To enable display
    /// <param name="requiredDisplayCount">The number of displays to be activate</param>
    /// </summary>
    static public void enableDisplay(int requiredDisplayCount)
    {
        int displayCount = Mathf.Min(Display.displays.Length, Display.displays.Length);
        if (displayCount > 1)
        {
            for (int i = 0; i < displayCount; i++)
            {
                Display.displays[i + 1].Activate();
            }
        }
    }
}
