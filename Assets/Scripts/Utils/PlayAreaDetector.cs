using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayAreaDetector : MonoBehaviour
{
    private KeyCode keyCodeForConfirm = KeyCode.None;

    private List<Vector3> corners = new List<Vector3>();

    private GameObject targetObject = null;

    private UnityEvent<Vector3> eventDefinedPoint = new UnityEvent<Vector3>();
    private UnityEvent<Vector3, Vector3> eventDefinedArea = new UnityEvent<Vector3, Vector3>();


    static private GameObject padGameObject = null;
    static private PlayAreaDetector pad;

    static private PlayAreaDetector shared()
    {
        if (padGameObject == null)
        {
            padGameObject = new GameObject("PlayAreaDetector-DontDestroyOnLoad");
            DontDestroyOnLoad(padGameObject);
            pad = padGameObject.AddComponent<PlayAreaDetector>();
        }
        return pad;
    }

    /// <summary>
    /// Stop defining the play area
    /// </summary>
    static public void stopDefinePlayArea()
    {
        PlayAreaDetector sharedInstance = PlayAreaDetector.shared();
        sharedInstance._stopDefinePlayArea();
    }

    /// <summary>
    /// Start defining the play area, two vector3 values will be returned by the passing through the given UnityAction callback.  
    /// </summary>
    /// <param name="trackerObj">The {GameObject} of the tracker</param>
    /// <param name="onAreaDefined">A callback function {(Vector3 v1, Vector3 v2)=>{})}, which will be fired after the play area is defined.</param>
    /// <param name="onPointDefined">A callback function {(Vector3 v)=>{})}, which will be fired after any point is defined. Optional. Default is null.</param>
    /// <param name="specificKeyCodeConfirm">The {KeyCode} as confirm key. Optional, KeyCode.Return is the default keycode if the code isn't given or none.</param>
    static public void startDefinePlayArea(GameObject trackerObj, UnityAction<Vector3, Vector3> onAreaDefined, UnityAction<Vector3> onPointDefined = null, KeyCode specificKeyCodeConfirm = KeyCode.Return)
    {
        PlayAreaDetector sharedInstance = PlayAreaDetector.shared();
        sharedInstance._startDefinePlayArea(trackerObj, onAreaDefined, onPointDefined, specificKeyCodeConfirm);
    }


    // Update is called once per frame
    void Update()
    {
        // Ensure the requried conditions are matched
        if (targetObject == null || keyCodeForConfirm == KeyCode.None || corners.Count >= 2)
        {
            return;
        }

        // Only do when confirm keycode key up
        if (Input.GetKeyUp(keyCodeForConfirm))
        {
            // Add the current position of the target object to the corners list
            corners.Add(new Vector3(targetObject.transform.position.x, 0.0f, targetObject.transform.position.z));

            // Invoke event to fire those callback functions which have been added to those events.
            eventDefinedPoint.Invoke(corners[corners.Count - 1]);
            if (corners.Count >= 2)
            {
                eventDefinedArea.Invoke(corners[0], corners[1]);
            }
        }
    }

    void OnGUI()
    {
        // Ensure the requried conditions are matched
        if (targetObject == null || keyCodeForConfirm == KeyCode.None || corners.Count >= 2)
        {
            return;
        }
        string msg = "Press " + keyCodeForConfirm.ToString() + " to define a crner by using current point (" + targetObject.transform.position.x + ", " + targetObject.transform.position.z + ")";

        corners.ForEach((Vector3 point) =>
        {
            msg += "\nAdded: (" + point.x + ", " + point.z + ")";
        });

        GUI.Box(new Rect(0, Screen.height - 50, Screen.width, 50), msg);
    }

    private void _stopDefinePlayArea()
    {
        targetObject = null;
        keyCodeForConfirm = KeyCode.None;
        corners.RemoveRange(0, corners.Count);
        eventDefinedArea.RemoveAllListeners();
        eventDefinedPoint.RemoveAllListeners();
    }



    private void _startDefinePlayArea(GameObject trackerObj, UnityAction<Vector3, Vector3> onAreaDefined, UnityAction<Vector3> onPointDefined = null, KeyCode specificKeyCodeConfirm = KeyCode.Return)
    {
        // Ensure the callback is not null before starting
        if (onAreaDefined == null)
        {
            Debug.LogError("Failed to start defining play area because onDefined should not be null.");
            return;
        }

        // Remove all corners
        if (corners.Count > 0)
        {
            corners.RemoveRange(0, corners.Count);
        }

        // Remove existing callback from event's listner, then add the given callback functions as the new event listenrs
        eventDefinedArea.RemoveAllListeners();
        eventDefinedArea.AddListener(onAreaDefined);
        eventDefinedPoint.RemoveAllListeners();
        if (onPointDefined != null)
        {
            eventDefinedPoint.AddListener(onPointDefined);
        }

        // Update the target object
        targetObject = trackerObj;

        // Update the keycode for confirm
        keyCodeForConfirm = specificKeyCodeConfirm != KeyCode.None ? specificKeyCodeConfirm : KeyCode.Return;
    }



}
