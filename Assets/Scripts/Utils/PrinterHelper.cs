﻿using System.Collections;
using System.Collections.Generic;
using LCPrinter;
using UnityEngine;
using Texture2DExtensions;

public class PrinterHelper : MonoBehaviour
{
    static private GameObject phGameObject = null;
    static private PrinterHelper ph = null;

    static private PrinterHelper shared()
    {
        if (PrinterHelper.phGameObject == null)
        {
            PrinterHelper.phGameObject = new GameObject();
            PrinterHelper.phGameObject.name = "PrinterHelperGameObject-DontDestroyOnLoad";
            DontDestroyOnLoad(PrinterHelper.phGameObject);
            PrinterHelper.ph = PrinterHelper.phGameObject.AddComponent<PrinterHelper>();
        }
        return PrinterHelper.ph;
    }

    /// <summary>
    /// Print a picture once
    /// </summary>
    /// <param name="picture">The given picture to be printed</param>
    static public void print(Texture2D picture)
    {
        PrinterHelper.shared().printPicture(picture);
    }

    private void printPicture(Texture2D picture)
    {
        StartCoroutine(asyncPrintPicture(picture));
    }

    private IEnumerator asyncPrintPicture(Texture2D picture)
    {
        if (picture != null)
        {
            Texture2D pictureCopy = picture.GetCopy();
            yield return new WaitForSeconds(1);
            Print.PrintTexture(pictureCopy.EncodeToPNG(), 1, "");
        }
        else
        {
            Debug.Log("Given picture is null");
        }
    }
}
