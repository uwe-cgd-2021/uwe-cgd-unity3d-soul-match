using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SoundEffectAndMusic
{
    PRESS_START,
    READY_BACKGROUND,
    PUZZLE_ELIMINATE,
    GAMEPLAY_BACKGROUND,
    WIN_BACKGROUND,
    LOSE_BACKGROUND,
    GAMEPLAY_CREATECOUPLE
}

public class PlaySound : MonoBehaviour
{
    private static Dictionary<SoundEffectAndMusic, string> PathOfSounds = new Dictionary<SoundEffectAndMusic, string>
    {
        {SoundEffectAndMusic.READY_BACKGROUND, "Sounds/Music/01_Opening/541689__ellary__jazzy-vibes-81-jazz-piano-medley"},
        {SoundEffectAndMusic.PRESS_START, "Sounds/SoundEffect/01_Opening/Confirm/61234__sapht__snes-startup"},
        {SoundEffectAndMusic.PUZZLE_ELIMINATE, "Sounds/SoundEffect/04_Gameplay/PuzzleEliminate/131662__bertrof__game-sound-correct-v2"},
        {SoundEffectAndMusic.GAMEPLAY_BACKGROUND, "Sounds/Music/03_Gameplay/PP078_trk003_DA_QUANDO_Ingo_Hassenstein_GEMA_50_539809510_Jochen_Engel_GEMA_50_507230196"},
        {SoundEffectAndMusic.GAMEPLAY_CREATECOUPLE, "Sounds/SoundEffect/004_Gameplay/CoupleCreated/380291__rhodesmas__ui-04"},
        {SoundEffectAndMusic.WIN_BACKGROUND, "Sounds/Music/05_win/GAS027_trk004_CAN_WE_FIX_IT_Alexander_Salter_APRA_100_179261936"},
        {SoundEffectAndMusic.LOSE_BACKGROUND, "Sounds/Music/04_Lose/HDMN556_trk002_FUNNY_BUSINESS_Madatian_Andre_100_ASCAP_716877308"},
    };

    private static Dictionary<SoundEffectAndMusic, string> NameOfClips = new Dictionary<SoundEffectAndMusic, string>
    {
        {SoundEffectAndMusic.READY_BACKGROUND, "541689__ellary__jazzy-vibes-81-jazz-piano-medley"},
        {SoundEffectAndMusic.PRESS_START, "61234__sapht__snes-startup"},
        {SoundEffectAndMusic.PUZZLE_ELIMINATE, "131662__bertrof__game-sound-correct-v2"},
        {SoundEffectAndMusic.GAMEPLAY_BACKGROUND, "PP078_trk003_DA_QUANDO_Ingo_Hassenstein_GEMA_50_539809510_Jochen_Engel_GEMA_50_507230196"},
        {SoundEffectAndMusic.WIN_BACKGROUND, "GAS027_trk004_CAN_WE_FIX_IT_Alexander_Salter_APRA_100_179261936"},
        {SoundEffectAndMusic.GAMEPLAY_CREATECOUPLE, "380291__rhodesmas__ui-04"},
        {SoundEffectAndMusic.LOSE_BACKGROUND, "HDMN556_trk002_FUNNY_BUSINESS_Madatian_Andre_100_ASCAP_716877308"},
    };

    private static PlaySound getPlaySound(GameObject target)
    {
        PlaySound playSound = null;
        if (target != null)
        {
            playSound = target.GetComponent<PlaySound>();
            if (playSound == null)
            {
                playSound = target.AddComponent<PlaySound>();
            }
        }
        return playSound;
    }

    private static AudioSource getAudioSource(PlaySound playSound)
    {
        AudioSource source = null;
        source = playSound.gameObject.AddComponent<AudioSource>();
        return source;
    }

    /// <summary>
    /// Play specific AudioClip
    /// </summary>
    /// <param name="sound">the sound</param>
    /// <param name="audioGameObject">which gameobject are using</param>
    /// <param name="Loop">will the clip loop(loop for a background music)</param>
    /// <param name="volume">volume(0 - 1)</param>
    public static void playAudio(SoundEffectAndMusic sound, GameObject audioGameObject, bool Loop = false, float volume = 1.0f)
    {
        PlaySound playSound = getPlaySound(audioGameObject);
        AudioSource audioSource = null;
        if (playSound != null)
        {
            audioSource = getAudioSource(playSound);
            audioSource.clip = Resources.Load<AudioClip>(PathOfSounds[sound]);
            audioSource.volume = volume;
            audioSource.loop = Loop;
            audioSource.Play();
        }
    }

    /// <summary>
    /// Stop playing audio
    /// </summary>
    /// <param name="sound">the sound</param>
    /// <param name="audioGameObject">which gameobject are using</param>
    public static void stopAudio(SoundEffectAndMusic sound, GameObject audioGameObject)
    {
        PlaySound playSound = null;
        playSound = audioGameObject.GetComponent<PlaySound>();
        if (playSound != null)
        {
            foreach (AudioSource source in audioGameObject.GetComponentsInChildren<AudioSource>())
            {
                if (source.clip.name == NameOfClips[sound])
                {
                    source.Stop();
                }
            }
        }
    }


    private void Update()
    {
        foreach (AudioSource source in gameObject.GetComponentsInChildren<AudioSource>())
        {
            if (source != null && source.isPlaying == false)
            {
                Destroy(source);
            }
        }
    }
}
