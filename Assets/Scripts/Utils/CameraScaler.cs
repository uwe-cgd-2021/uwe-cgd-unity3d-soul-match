using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScaler : MonoBehaviour
{
    /// <summary>
    /// Scale the camera to focus on a area(ONLY for orthographic camera and only the camera view, width have to be longer than height) this will not scale the UI
    /// </summary>
    /// <param name="camera">Which camera have to set</param>
    /// <param name="TargetPosition">The middle point of the area that camera </param>
    /// <param name="targetRotation">The rotation of that camera </param>
    /// <param name="height">The height of the area that the camera have to focus on (in unity unit[a default cube is one unit]) </param>
    /// <param name="cropLeftAndRight"> Do you need to crop the width of the camera</param>
    public static void SetCamera(Camera camera, Vector3 TargetPosition, Quaternion targetRotation, float height, bool cropLeftAndRight = false)
    {
        camera.orthographic = true;
        camera.rect = new Rect(0f, 0f, 1.0f, 1.0f);
        camera.transform.position = TargetPosition;
        camera.transform.localRotation = targetRotation;
        camera.orthographicSize = height / 2;
        if (cropLeftAndRight == true)
        {
            float widthOfViewPort = 1 / (height * camera.aspect);
            camera.rect = new Rect(0f, 0f, widthOfViewPort, 1.0f);
        }
    }
    /// <summary>
    /// Scale the camera to focus on a area(ONLY for orthographic camera and only the camera view, width have to be longer than height) this will not scale the UI
    /// </summary>
    /// <param name="camera">Which camera have to set</param>
    /// <param name="height">The height of the area that the camera have to focus on (in unity unit[a default cube is one unit]) </param>
    /// <param name="cropLeftAndRight"> Do you need to crop the width of the camera</param>
    public static void SetCamera(Camera camera, float height, bool cropLeftAndRight = false)
    {
        camera.orthographic = true;
        camera.rect = new Rect(0f, 0f, 1.0f, 1.0f);
        camera.orthographicSize = height / 2;
        if (cropLeftAndRight == true)
        {
            float widthOfViewPort = 1 / (height * camera.aspect);
            camera.rect = new Rect(0f, 0f, widthOfViewPort, 1.0f);
        }
    }

    /// <summary>
    /// Scale the camera to focus on a area(ONLY for orthographic camera and only the camera view, width have to be longer than height) this will not scale the UI
    /// </summary>
    /// <param name="camera">Which camera have to set</param>
    /// <param name="height">The height of the area that the camera have to focus on (in unity unit[a default cube is one unit]) </param>
    /// <param name="width"> The width of the area(in Unity unit)</param>
    public static void SetCamera(Camera camera, float height, float width)
    {
        camera.orthographic = true;
        camera.rect = new Rect(0f, 0f, 1.0f, 1.0f);

        if (height >= width)
        {
            camera.orthographicSize = height / 2;
            float widthOfViewPort = (1 / (height * camera.aspect)) * width;
            widthOfViewPort = Mathf.Floor(widthOfViewPort * 100) / 100;
            camera.rect = new Rect(0f, 0f, widthOfViewPort, 1.0f);
        }
        else
        {
            camera.orthographicSize = height / 2;
            float heightOfViewPort = height / width * camera.aspect;
            heightOfViewPort = Mathf.Floor(heightOfViewPort * 100) / 100;
            camera.rect = new Rect(0f, 0f, 1.0f, heightOfViewPort);
        }
    }


    /// <summary>
    /// Scale the camera to focus on a area(ONLY for orthographic camera and only the camera view, width have to be longer than height) this will not scale the UI
    /// </summary>
    /// <param name="camera">Which camera have to set</param>
    /// <param name="TargetPosition">The middle point of the area that camera </param>
    /// <param name="targetRotation">The rotation of that camera </param>
    /// <param name="height">The height of the area that the camera have to focus on (in unity unit[a default cube is one unit]) </param>
    /// <param name="width"> The width of the area(in Unity unit)</param>
    public static void SetCamera(Camera camera, Vector3 position, Quaternion targetRotation, float height, float width)
    {
        camera.orthographic = true;
        camera.rect = new Rect(0f, 0f, 1.0f, 1.0f);
        camera.transform.position = position;
        camera.transform.localRotation = targetRotation;

        if (height >= width)
        {
            camera.orthographicSize = height / 2;
            float widthOfViewPort = (1 / (height * camera.aspect)) * width;
            camera.rect = new Rect(0f, 0f, widthOfViewPort, 1.0f);
        }
        else
        {
            camera.orthographicSize = height / 2;
            float heightOfViewPort = height / width * camera.aspect;
            camera.rect = new Rect(0f, 0f, 1.0f, heightOfViewPort);
        }
    }


}
