# Introduction

This is a Unity based game project for UWE CGD Component B - Game Development In Unity 3D.

## Team members

- Lam Wai Sang, Sunny (217061724)
- Lau Kwun Man, Edwin (217061736)
- Lau Ngo Ting, Teddy (217061564)
- Pang Fuk Sang, Andy (217060266)

---

# Development

## Development Environment

- Unity 2020.3.22f1
- VSCode
- [Prettier - Code formatter](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
- Git
- [Git LFS](https://git-lfs.github.com/)

## Hardware configuration

- HTC VIVE
- VR Ready PC
  - Intel Core i5-4590/AMD FX 8350 equivalent or better
  - NVIDIA GeForce GTX 1060, AMD Radeon R9 480 equivalent or better
  - 8 GB RAM or more
  - 64-bit Windows 10

## Git Flow

Follow the approach of **Gif Flow** to name your branches on this repository. [Read More about Git Flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)

- There are 2 main branches `develop` and `master`.

- If there is no special requirement, please always create the branch as feature branch from `develop` **NOT** `master`.

- You should name your feature branch by following the pattern like `feature/something-you-are-going-to-do`.

- Only commit the changes that you've made. Avoid adding files by selecting all changed files. Some files might be changed by the system or other reasons, pay attention to those files, only commit them if you know where are they from and you need everyone has the changes in the future.

- When your `feature` is completed according to your ticket, it has to be reviewed and apporoved by at least one teammate via **Pull Request** before merging to the `develop` branch.

- Test your code and check if you have pushed your code before creating Pull Request. Please also mention how to test your feature on the description of the PR. If there is specific ticket for that feature branch, please also place the link of the ticket on the description of the PR.
